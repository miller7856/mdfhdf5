# Overview #

The mdfHdf5 library provides tools to convert between MDF objects and HDF5 file format. MdfHdf5 is cross-platform library, implemented in Python and Matlab. The HDF5 file provides consistent data representation of the MDF object on both platforms.   

# Handling Types #
To ensure consistent hdf5 format across all platforms, you must include the `mdf_hdf5_types` object in your `mdf`. 
The following table shows default mapping and optional mappings. No action is needed for default mappings but you'll most likely need to use the optional mappings in Matlab.
See examples below these tables. Notice the different needs for `mdf_hdf5_types` in Python vs Matlab. 

Matlab | Default | Option(s)
:-----|:-------|:-------                           
numeric array 1x1 | MDF_HDF5_NUM | see List A          
numeric array mxn | MDF_HDF5_NARRAY_NUM | see List A
char array | MDF_HDF5_STR | n/a
cell array strings | MDF_HDF5_ARRAY_STR | MDF_HDF5_ARRAY_STR_UNI
cell array multiple types | MDF_HDF5_ARRAY_UNTYPED | n/a
struct 1x1 | MDF_HDF5_DICT | MDF_HDF5_ARRAY_DICT
struct array | MDF_HDF5_ARRAY_DICT | n/a

Python | Default | Option(s)
:-----|:-------|:------- 
int/float | MDF_HDF5_INT | n/a
list of int/float | MDF_HDF5_NARRAY_NUM | see List A
numpy array | MDF_HDF5_NARRAY_NUM | see List A
dictionary | MDF_HDF5_DICT | n/a
list of dictionairs | MDF_HDF5_ARRAY_DICT | n/a
list of multiple types | n/a | MDF_HDF5_ARRAY_UNTYPED
list of strings | MDF_HDF5_ARRAY_STR_UNI | n/a
nested list of strings | MDF_HDF5_ARRAY_STR | n/a
string | MDF_HDF5_STR | n/a


MDF_HDF5_TYPES | Definitions | List A
:-------------|:-----------|:------- 
MDF_HDF5_NUM | scalar number (double) | The following are applicable
MDf_HDF5_ARRAY_NUM | array/matrix of double | MDF_HDF5_ARRAY_INT
MDF_HDF5_ARRAY_NUM_UNI | unidimensional array/matrix of double | MDF_HDF5_ARRAY_INT_UNI
MDf_HDF5_STR | string | MDF_HDF5_NARRAY_INT
MDF_HDF5_ARRAY_STR | array of strings | MDF_HDF5_NARRAY_INT_UNI
MDF_HDF5_ARRAY_STR_UNI | unidimensional array strings |
MDF_HDF5_DICT | group in hdf5 file |
MDF_HDF5_ARRAY_DICT | group in hdf5 file with each element of array as subgroup |
MDF_HDF5_NARRAY_NUM | numpy array |
MDF_HDF5_NARRAY_NUM_UNI | unidemsional numpy array |
MDF_HDF5_ARRAY_UNTYPED | untyped array


Python will add the following to `mdf_hdf5_types`, if nothing has been specified yet.

Case | MDF_HDF5_TYPES
:----|:--------------
list of dictionary with one element | MDF_HDF5_ARRAY_DICT
unidimensional list of strings | MDF_HDF5_ARRAY_STR_UNI
unidimensional lisr of nums | MDF_HDF5_NARRAY_NUM_UNI
scalar num | MDF_HDF5_NUM

## Examples ##
Due to the fact Matlab doesn't distinguish between scalars and 1x1 arrays, extra information is needed in the mdf object when writing to hdf5 in Matlab. 
In the top level of the mdf object create/update `mdf_hdf5_types`. 
Once the extra informatin is added the first time, it'll be included in the mdf and will not need to be specified everytime. Notice `_x_` is used to replace `/` when defining a path in `mdf_hdf5_types`.
### 1x1 ###
By default any 1x1 is treated like a scalar. If you wish to treat any 1x1 as an array you must specify. 
The python library does the following automatically and these cases are for Matlab.
#### Case 1 1x1 ####
    mdf_example = [
        {
            mdf_ex: 'example'
        }
    ]
If the path to `mdf_example` is `/path/to/mdf_example` to save this as an array add the following to `mdf_hdf5_types`:

    mdf_hdf_types = {
        path_x_to_x_mdf_example: 'MDF_HDF5_ARRAY_DICT'
    }
#### Case 2 1x1 ####
    mdf_example = [ 1 ]
If the path to `mdf_example` is `/path/to/mdf_example` to save this as an array add the following to `mdf_hdf5_types`:

    mdf_hdf_types = {
        path_x_to_x_mdf_example: 'MDF_HDF5_ARRAY_INT_UNI'
    }
### Arrays ###

#### Case 1 list ####
By defulat Python opens all datasets as numpy array. To have an array treated as a list when read in python, you must specify when writing in Matlab and Python.

    mdf_example = [ 1 2 3 4 5 ]
If the path to `mdf_example` is `/path/to/mdf_example` to save this as an array add the following to `mdf_hdf5_types`:

    mdf_hdf_types = {
        path_x_to_x_mdf_example: 'MDF_HDF5_ARRAY_INT'
    }
#### Case 2 unidimensional ####
By default matlab adds an extra dimension to all unidimensional arrays. To have an array treated as unidimensional in python, you must specify when writing in Matlab. 
Python will automatically do this when writing unidimensional arrays to hdf5.

    mdf_example = [ 1 2 3 4 5 ]
If the path to `mdf_example` is `/path/to/mdf_example` to save this as an array add the following to `mdf_hdf5_types`:

    mdf_hdf_types = {
        path_x_to_x_mdf_example: 'MDF_HDF5_ARRAY_INT_UNI'
    }


# Indexing datasets #
`matlab(1, 3, 2) == python[1, 0, 2]` will return the same elemet. If you are unfamiliar, the first element of a Python array is the 0th element and the first element of a Matlab array is the 1st element.
To avoid confusion, let's add 1 to each of `python`'s index i.e `python[2, 1, 3]`. Now it's easy to see the relationship between these two indexs. In this example, index `2` is the third dimension. 
Matlab appends extra dimensions to the end while Python appends them to the begining. The index `1` is the row and `3` is column. 

# Testing #
### Cross Platform ###
Run `python run_test.py` in `/crossPlatform`.
### Matlab ###
Run `mdfHdf5Test` in `/mMdfHdf5/unitTest`.
### Python ###
Run `python mdfHdf5Test.py` in `/pyMdfHdf5/unitTest`.

# API #

## Matlab ##
- - -
### write ###
Writes and MDF object to HDF5

    Params
        structure - struct
            MDF object in memory 
        hdf5_filename - string
            MDF output HDF5 file 
        overwrite - boolean
            set to true to overwrite file
    write(structure, hdf5_filename, overwrite);
- - -   
### read ###
Reads MDF object from HDF5   

    Params
        hdf5_filename - string
            file where MDF is saved
    Returns
        MDF_object - struct 
    read(hdf5_filename)
- - -  
### read_partial ###
Reads specified groups of MDF object from HDF5

    Params
        names - char or cell array 
            paths to read
        hdf5_filename - string 
            file where MDF is saved
    Returns
        MDF_object - struct or struct array
            Partial MDF object of specified groups
    read_partial(paths, hdf5_filename)
- - -
### write_partial ###
    Params
        mdf -  struct or struct array
            the mdf to be written
        paths - char or cell array
            path of rewrite used for deleting old mdf
        hdf5_filename - char
            name of mdf file
        mdf_hdf5_types - struct
            extra information for writing - this will be written to the hdf5 file too
    write_partial(mdf, paths, hdf5_filename, mdf_hdf5_types)
## Python ##
- - -
### write ###
Writes and MDF object to HDF5

    Params
        structure - dict
            MDF object in memory 
        hdf5_filename - string
            MDF output HDF5 file 
    write(structure, hdf5_filename);
- - -   
### read ###
Reads MDF object from HDF5   

    Params
        hdf5_filename - string
            file where MDF is saved
    Returns
        MDF_object - dict
    read(hdf5_filename)
- - -  
### read_partial ###
Reads specified groups of MDF object from HDF5

    Params
        paths - string or list of strings
            paths to what to read
        hdf5_filename - string 
            file where MDF is saved
    Returns
        MDF_object - dict
            Partial MDF object of specified groups
    read_partial(paths, hdf5_filename)
    
### write_partial ###
Writes specified groups of MDF to HDF5

    Params
        mdf - dict or list of dicts
            the mdf to be written
        paths - string or list of strings
            path of rewrite used for deleting old mdf
        hdf5_filename - string
            name of mdf file
        mdf_hdf5_types - dict
            extra information for writing - this will be written to the hdf5 file too
    write_partial(mdf, paths, hdf5_filename, mdf_hdf5_types)
- - -