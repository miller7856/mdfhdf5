classdef mdfHdf5
   properties (Constant)
    MDF_HDF5_TYPES = 'mdf_hdf5_types';
    MDF_HDF5_TYPES_PATH = '/mdf_hdf5_types';
    MDF_HDF5_INT = 'MDF_HDF5_NUM';
    MDF_HDF5_ARRAY_INT = 'MDF_HDF5_ARRAY_NUM';
    MDF_HDF5_ARRAY_INT_UNI = 'MDF_HDF5_ARRAY_NUM_UNI';
    MDF_HDF5_NARRAY_INT = 'MDF_HDF5_NARRAY_NUM';
    MDF_HDF5_NARRAY_INT_UNI = 'MDF_HDF5_NARRAY_NUM_UNI';
    MDF_HDF5_ARRAY_DICT = 'MDF_HDF5_ARRAY_DICT';
    MDF_HDF5_ARRAY_STR = 'MDF_HDF5_ARRAY_STR';
    MDF_HDF5_ARRAY_STR_UNI = 'MDF_HDF5_ARRAY_STR_UNI';
    MDF_HDF5_ARRAY_UNTYPED = 'MDF_HDF5_ARRAY_UNTYPED';
    PATH_INDICATOR = '_x_';
    ARRAY_ELE_BASE_NAME = 'mdf_hdf5_ele_';
    HDF5_DOUBLE = 'H5T_NATIVE_DOUBLE';
    HDF5_STRING = 'H5T_C_S1';
    DIMENSIONS = 'dimensions';
    EMPTY_CELL_ARRAY = 'EMPTY_CELL_ARRAY';
    EMPTY_STRUCT = 'EMPTY_STRUCT';
   end
    methods(Static)
        function r = write(structure, hdf5_filename, trunc)
            fcpl = H5P.create('H5P_FILE_CREATE');
            fapl = H5P.create('H5P_FILE_ACCESS');
            if trunc
                f = H5F.create(hdf5_filename, 'H5F_ACC_TRUNC', fcpl, fapl);
            else
                f = H5F.open(hdf5_filename,'H5F_ACC_RDWR','H5P_DEFAULT');
            end
            % open mdf_hdf5_types if non exist create one
            try
                mdf_hdf5_types = structure.(mdfHdf5.MDF_HDF5_TYPES);
            catch
                mdf_hdf5_types = struct();
                structure.(mdfHdf5.MDF_HDF5_TYPES) = mdf_hdf5_types;
            end
            start_path = '';
            [mdf_hdf5_types, errors] = construct_hierarchy(structure, f, start_path, mdf_hdf5_types, true);
            construct_hierarchy(struct(mdfHdf5.MDF_HDF5_TYPES, mdf_hdf5_types), f, start_path, {}, false);
            H5F.close(f);
            r = errors;
        end 
        
        function r = read(hdf5_filename)
            try
                mdf_hdf5_types = convert_to_mdf(h5info(hdf5_filename, mdfHdf5.MDF_HDF5_TYPES_PATH), hdf5_filename, {});
            catch
               mdf_hdf5_types = {}; 
            end
            r = convert_to_mdf(h5info(hdf5_filename), hdf5_filename, mdf_hdf5_types);
        end
        
        % Writes mdf(s) to specified path(s) in given hdf5 file
        % Params
        %   mdf - char or cell array - mdf to write
        %   paths - char or cell array - desired group to write to
        %   hdf5_filename - char - location of hdf5 file
        % If writing multiple mdf objects length(mdf) == length(paths) and
        % each mdf object must have a corresponding path on same index 
        function r = write_partial(mdf, paths, hdf5_filename, mdf_hdf5_types)
            errors = {};
            start_path = '';
            f = H5F.open(hdf5_filename,'H5F_ACC_RDWR','H5P_DEFAULT');
            if iscell(mdf)
                for i = 1:length(paths)
                    path = char(strcat('/', paths(i)));
                    try
                        H5L.delete(f, path,'H5P_DEFAULT');
                    end
                    [updated_mdf_hdf5_types, error] = construct_hierarchy(mdf{i}, f, start_path, mdf_hdf5_types, true);
                    mdf_hdf5_types = combine_mdf_hdf5_types(mdf_hdf5_types, updated_mdf_hdf5_types);
                    if ~isempty(error)
                        errors = [errors, error];
                    end
                end    
            else
                path = char(strcat('/', paths));
                try
                    H5L.delete(f, path,'H5P_DEFAULT');
                end
                [updated_mdf_hdf5_types, errors] = construct_hierarchy(mdf, f, start_path, mdf_hdf5_types, true);
                mdf_hdf5_types = combine_mdf_hdf5_types(mdf_hdf5_types, updated_mdf_hdf5_types);
            end
            if ~isempty(mdf_hdf5_types)
                construct_hierarchy(struct(mdfHdf5.MDF_HDF5_TYPES, mdf_hdf5_types), f, start_path, [], false);
            end
            H5F.close(f);
            r = errors;
        end
        
        % Reads specified path(s) from given hdf5 file
        % Params
        %   paths - char or cell array - desired group to read
        %   hdf5_filename - char - location of hdf5 file
        % Returns - struct for single path input, cell array of structs for
        % multiple path input
        function r = read_partial(paths, hdf5_filename)
            mdf = struct([]);
            mdf_builder = struct([]);
            try
               mdf_hdf5_types = convert_to_mdf(h5info(hdf5_filename, mdfHdf5.MDF_HDF5_TYPES_PATH), hdf5_filename, {});
            catch
               mdf_hdf5_types = {}; 
            end
            % check for multiple inputs
            if iscell(paths)
                mdfs = cell(size(paths));
                for i = 1:length(paths)
                    path = char(paths(i));
                    fields = strsplit(path, '/');
                    depth = length(fields);
                    for j = depth:-1:1
                        field = char(fields(j));
                        % convert group at end of the path to mdf
                        if j == depth
                            mdf_builder = struct(field, convert_to_mdf(h5info(hdf5_filename, strcat('/', path)), hdf5_filename, mdf_hdf5_types));
                        % build path into struct
                        else
                            mdf_builder = struct(field, mdf_builder);
                        end
                    end
                    % add struct to cell array
                    mdfs{i} = mdf_builder;
                    mdf_builder = struct([]);
                end
                r = mdfs;
            else
                fields = strsplit(paths, '/');
                depth = length(fields);
                for j = depth:-1:1
                    field = char(fields(j));
                    if j == depth
                        mdf_builder = struct(field, convert_to_mdf(h5info(hdf5_filename, strcat('/', paths)), hdf5_filename, mdf_hdf5_types));
                    else
                        mdf_builder = struct(field, mdf_builder);
                    end
                end
                mdf(1).(field) = mdf_builder.(field);
                r = mdf;
            end
        end
            
    end
end

% Convert_to_mdf helper function
% Params
%   group_names [cell array] array of full paths for each group
% Returns list of group names without full path
function r1 = get_names(group_names)
    dim = size(group_names);
    names = cell(1, dim(2)-1);
    for i = 1:dim(2)
        split_name = strsplit(group_names{i}, '/');
        names(i) = split_name(end);
    end
    r1 = names;
end
% get all names of attr and datasets
function r1 = get_names_attr_dset(hdf5)
    names = {};
    if size(hdf5.Attributes) > 0
        for i = 1:size(hdf5.Attributes)
            names{i} = hdf5.Attributes(i).Name;
        end
    else
       i = 0; 
    end
    if size(hdf5.Datasets) > 0
        for k = 1:size(hdf5.Datasets)
            names{k+i} = hdf5.Datasets(k).Name;
        end
    end
    r1 = string(names);
end

function r = open_dataset(dset, hdf5, hdf5_filename)
        % check if at highest level to avoid // in dset name
        if strcmp(hdf5.Name, '/')
            dsetname = strcat(hdf5.Name, dset.Name);
        else
            dsetname = strcat(hdf5.Name, '/', dset.Name);
        end
        % read data set from file
        data = h5read(hdf5_filename, dsetname);
        if length(size(data)) > 2
            perm = 1:length(size(data));
            shape = [perm(2), perm(1), fliplr(perm(3:end))];
        else
            perm = 1:length(size(data));
            shape = [perm(2), perm(1)];
        end
       r =  permute(data, shape);
end

% make ele names a number so they can be sorted
function name_index = get_names_as_indexes(names) 
    name_index = [];
    for i = 2:length(names)
        name_split = split(names{i}, '_');
        name_index(i) = str2num(strtrim(strjoin(split(name_split(end), 'x'), '')));
    end
    name_index = sort(name_index);
end

function mdf = convert_to_mdf(hdf5, hdf5_filename, mdf_hdf5_types)
    mdf = [];
    mapping = false;
    % check for mapping
    path = strrep(hdf5.Name(2:end), '/', mdfHdf5.PATH_INDICATOR);
    if isfield(mdf_hdf5_types, path)
        mapping = mdf_hdf5_types.(path);
    end
    
    names = get_names_attr_dset(hdf5);
    % handle cell array
    if strcmp(mapping, mdfHdf5.MDF_HDF5_ARRAY_UNTYPED) || length(names) > 1 && contains(names(2), mdfHdf5.ARRAY_ELE_BASE_NAME)
        dset = hdf5.Datasets;
        attr = hdf5.Attributes;
        % init cell array
        arr_shape = open_dataset(dset(1), hdf5, hdf5_filename);
        if length(arr_shape) > 1
            mdf = cell(fliplr(arr_shape));
        else
            % handle uni-dimensional shape
            mdf = cell(1, arr_shape);
        end
       
        % init start index and max_index
        index = arr_shape;
        max_index = arr_shape;
        for k = 1:length(arr_shape)
            index(k) = 0;
            max_index(k) = max_index(k)-1;
        end
        % build cell array
        name_index = 2;
        dataset_index = 2;
        attr_index = 1;
        dset_names = get_names_as_indexes({dset.Name});
        attr_names = get_names_as_indexes({attr.Name});
        for k = 1:prod(arr_shape)
            ele_name = strcat(mdfHdf5.ARRAY_ELE_BASE_NAME, join(strtrim(split(num2str(index), '  ')), 'x'));
            % must take each element name and convert the index to a number because
            % matlab cannot sort strings very well
            ele_name_index = str2num(strjoin(strtrim(split(num2str(index), '  ')), ''));
            if name_index <= length(names) && ~isempty(find(contains(names, ele_name)))                                     
                if dataset_index <= length(dset)  && isequal(dset_names(dataset_index), ele_name_index)
                    mdf{k} = open_dataset(dset(dataset_index), hdf5, hdf5_filename);
                    dataset_index = dataset_index + 1;
                % handle attr
                elseif attr_index <= length(attr) && isequal(attr_names(attr_index), ele_name_index)
                    at = attr(attr_index).Value;
                    if isnumeric(at)
                        mdf{k} = at;
                    else
                        % Cast value as char array
                        mdf{k} = char(at);
                    end                    
                    attr_index = attr_index + 1;

                end
                name_index = name_index + 1;
            % add empty matrix for missing indexes
            else
                mdf{k} = [];
            end
            index = increment_index(index, max_index);
        end
        % reshape cell array if needed
        if length(arr_shape) > 1
            mdf = fliplr(rot90(mdf, -1));
       end
    else
        
        %handle attributes
        attrs = hdf5.Attributes;
        attrs_dims = size(attrs);
        % Check this hdf5 as attributes
        if attrs_dims(1) > 0
            for j = 1:attrs_dims(1)
                attr = attrs(j);
                % Check for numeric value
                if isnumeric(attr.Value)
                    mdf.(attr.Name) = attr.Value;
                else
                    if strcmp(attr.Value, mdfHdf5.EMPTY_CELL_ARRAY)
                       mdf.(attr.Name) = {}; 
                    elseif strcmp(attr.Value, mdfHdf5.EMPTY_STRUCT)
                         mdf.(attr.Name) = struct();
                    else
                        % Cast value as char array
                        mdf.(attr.Name) = char(attr.Value);
                    end
                end
            end
        end
        
        %handle datasets
        dset = hdf5.Datasets;
        % Check that this hdf5 has datasets
        if ~isempty(dset)
            dim_d = size(dset);
            for k = 1:dim_d(1)
                mdf.(dset(k).Name) = open_dataset(dset(k), hdf5, hdf5_filename);
            end
        end
        
        %handle groups
        groups = hdf5.Groups;
        % Check that the HDF5 has groups
        if ~isempty(groups)
            % Get group names
            group_names = {groups.Name};
            names = get_names(group_names);
            names_len = length(names);
            % Check if names indicate groups belong in an array
            if contains(names(1), mdfHdf5.ARRAY_ELE_BASE_NAME)
                list = struct([]);
                % Build array of objects
                for i = 1:names_len
                    % Make recursive call for group
                    temp = convert_to_mdf(groups(i), hdf5_filename, mdf_hdf5_types);
                    temp_fields = fieldnames(temp);
                    % Add new item (temp) to the array
                    for m = 1:size(temp_fields)
                        field = char(temp_fields(m));
                        list(i).(field) = (temp.(field));
                    end
                end
                mdf = list;
                % Make recursive call for each group
            else
                for i = 1:names_len
                    mdf.(char(names(i))) = convert_to_mdf(groups(i), hdf5_filename, mdf_hdf5_types);
                end
            end
        end
    end
end

% Helper function for construct_hierarchy to write attrubutes
% Params
%   hdf5 - [double] - file identifier of open HDF5 file
%   attr_name [char array] - name of attribute
%   data [char array|double] - value of attribute
%   type [char array] - appropriate HDF5 type
function write_attr(hdf5, attr_name, data, type)
    acpl_id = H5P.create('H5P_ATTRIBUTE_CREATE');
    space_id = H5S.create('H5S_SCALAR');
    type_id = H5T.copy(type);
    % If the type isn't DOUBLE extra space must be allocated. 
    if ~strcmp(type, mdfHdf5.HDF5_DOUBLE)
        len = length(data);
        H5T.set_size(type_id, len);
    end
    % try to open attr if exist
    try
        attr_id = H5A.open(hdf5, attr_name);
    catch
        % Create
        attr_id = H5A.create(hdf5, attr_name,type_id,space_id,acpl_id);
    end
    %Write attribute and clean up
    H5A.write(attr_id, type_id, data);
    H5A.close(attr_id);
    H5S.close(space_id);
    H5P.close(acpl_id);
    H5T.close(type_id);
end

% Helper function of constrcut hierarchy,
% Params
%   path [string] hdf5 path to group
%   name [char array] name of group
%   parent_id [int] ref to parent 
function r = create_or_open_group(path, name, parent_id)
    plist = 'H5P_DEFAULT';
    try
        group = H5G.open(parent_id, path);
    catch
        group = H5G.create(parent_id, name, plist, plist, plist);
    end
    r = group;
end

% Helper function of construct_hierarchy
% Params
%   hdf5 - hdf5 object
%   item - data to write
%   name - naem of data
%   type - hdf5 datatype
function write_dataset(hdf5, item, name, type, dims)
    plist = 'H5P_DEFAULT';
    data_length = length(item);
    datatypeID = H5T.copy(type);
    dcpl = H5P.create('H5P_DATASET_CREATE');
    
    % reshape data
    perm = 1:length(dims);
    if length(perm) > 2
        if length(perm) > 3
            shape = [perm(2), perm(1), fliplr(perm(3:end))];
            data = permute(item, shape);
            dims = fliplr(size(data));
        else 
            shape = [perm(2), perm(1),fliplr(perm(3:end))];
            data = permute(item, shape);
            dims = size(data);
        end
    else
       shape = [perm(2), perm(1)];
       data = permute(item, shape);
    end
    dataspaceID = H5S.create_simple(length(dims), dims, []); 
    % Check if dataset exist
    try
        dataset_id = H5D.open(hdf5, name);
    catch
        % Check that dataset isn't empty
        if data_length > 0
            % numeric dataset config
            if isnumeric(item)
                % Configure data compression 
                H5P.set_deflate(dcpl,4);
                H5P.set_chunk(dcpl,dims);
            % string dataset config
            elseif iscell(item)
                % Set size for strings
                H5T.set_size(datatypeID,'H5T_VARIABLE'); 
            end
            dataset_id = H5D.create(hdf5, name, datatypeID, dataspaceID, dcpl);
        else
            % Create placeholder to represent an empty dataset []
            dataset_id = H5D.create(hdf5, name, datatypeID, dataspaceID, plist); 
        end
    end
    % Write data and clean up
    H5D.write(dataset_id,'H5ML_DEFAULT','H5S_ALL','H5S_ALL',plist, data);
    H5D.close(dataset_id);
    H5S.close(dataspaceID);
    H5T.close(datatypeID);
end

function r = increment_index(index, max_index)
    % iterate each dimension
    for i = 1:length(max_index)
        if index(i) == max_index(i)
            if i == length(max_index)
                for k = 0:length(max_index)-1
                    if index(end-k) == max_index(end-k)
                        index(end-k) = 0; 
                    else
                        index(end-k) = index(end-k) + 1;
                        break
                    end
                end
            end
            % should be less if not equal
        elseif index(i) < max_index(i)
            % increment if its the last dimension
            if i == length(max_index)
                index(i) = index(i) + 1;
            end
        end
    end
    r = index;
end

% adds any new fields to mdf_hdf5_types
function r = combine_mdf_hdf5_types(mdf_hdf5_types, updated_mdf_hdf5_types)
    if ~isempty(updated_mdf_hdf5_types)
        keys = fieldnames(updated_mdf_hdf5_types);
        for i = 1:length(keys)
            mdf_hdf5_types.(keys{i}) = updated_mdf_hdf5_types.(keys{i});  
        end
    end
    r = mdf_hdf5_types;
end

% append leading zeros to index
function r = get_ele_number(num, len) 
    num = num2str(num);
    curr_places = length(split(num, ''))-2;
    places = length(split(num2str(len), ''))-2;
    while curr_places < places
       num = strcat('0', num);
       curr_places = curr_places + 1; 
    end
    r = num;
end

% Helper function that builds the hierarchy of the HDF5 using recursion.
% Params
%   structure [struct] - MDF Object in memory
%   hdf5 [double] - File identifier of open HDF5 file/group ID
%   path [char array] - tracking full path of hdf5
%   mdf_hdf5_types [stuct] - struct of mappings
%   check [bool] - true means check for mappings, false is used when
%   writing mdf_hdf5_types to the file
function [mdf_hdf5_types, errors] = construct_hierarchy(structure, hdf5, path, mdf_hdf5_types, check)
    errors = {};
    % toggle mapping
    mapping = false;
    % set mapping
    map = '';
    % Iterate through all top level fields of structure
    keys = fieldnames(structure);
    for i = 1:size(keys)
        % current item to be written
        item = structure.(keys{i});
        dim = size(item);
        name = char(keys(i));
        len = length(item);
        
        % build full path to current item
        new_path = strcat(path, '/', name); 
        if check
            % build path that would be in mdf_hdf5_types
            x_path = strrep(new_path(2:end), '/', mdfHdf5.PATH_INDICATOR);
            if isfield(mdf_hdf5_types,x_path)
                % set mapping
                mapping = true;
                map = mdf_hdf5_types.(x_path);
            end
        end
        
        % check if is string
        if ischar(item)
            % Check for fields that have no value
            if dim(2) > 0
                write_attr(hdf5, name, item, mdfHdf5.HDF5_STRING); 
            else
                error_msg = strcat(new_path, ' has no value to write as attribute');
                errors{length(errors)+1} = error_msg;
            end
            
        % check if is array of nums
        elseif isnumeric(item)
            % check if item has special mapping
            if mapping
               % write as dataset
               if strcmp(map, mdfHdf5.MDF_HDF5_ARRAY_INT) || strcmp(map, mdfHdf5.MDF_HDF5_NARRAY_INT) || strcmp(map, mdfHdf5.MDF_HDF5_ARRAY_INT_UNI) || strcmp(map, mdfHdf5.MDF_HDF5_NARRAY_INT_UNI)
                   write_dataset(hdf5, item, name, mdfHdf5.HDF5_DOUBLE, dim);
               % write as attribute
               elseif strcmp(map, mdfHdf5.MDF_HDF5_INT)
                   write_attr(hdf5, name, double(item), mdfHdf5.HDF5_DOUBLE);
               % mapping unknown
               else
                   error_msg = strcat('Invalid mapping ', map);
                   errors{length(errors)+1} = error_msg;
               end
            % no mapping
            else
                % by default any vector of length 1 is a attribute
                if len == 1
                    write_attr(hdf5, name, double(item), mdfHdf5.HDF5_DOUBLE);
                else
                    write_dataset(hdf5, item, name, mdfHdf5.HDF5_DOUBLE, dim);
                end
            end
        % check if is array of strings
        elseif iscell(item)
            if ~isempty(item)
                % determine contents of cell array
                string_array = true;
                for x = 1:len
                   if ~ischar(item{x})
                       string_array = false;
                   end
                end
                % handle cell of strings
                if string_array
                    %write array of strings as dataset
                    write_dataset(hdf5, item, name, mdfHdf5.HDF5_STRING, dim)
                else
                    % add to mdf_hdf5 types
                    mdf_hdf5_types.(x_path) = mdfHdf5.MDF_HDF5_ARRAY_UNTYPED;
                    % create group to create array elements on
                    group = create_or_open_group(new_path, name, hdf5);
                    % transform dimensions to match python
                    max_index = [fliplr(dim(3:end)), dim(1:2)];
                    % create dataset to record the shape of array
                    write_dataset(group, max_index, mdfHdf5.DIMENSIONS, mdfHdf5.HDF5_DOUBLE, size(max_index));
                    % add uni dimension type
                    dimensions_path = strcat(x_path, mdfHdf5.PATH_INDICATOR, mdfHdf5.DIMENSIONS);
                    mdf_hdf5_types.(dimensions_path) = mdfHdf5.MDF_HDF5_NARRAY_INT_UNI;
                    % set start index
                    index = [];
                    for m = 1:length(max_index)
                        index(m) = 0;
                        % transform index to start from 0
                         max_index(m) = max_index(m)-1;
                    end
                    % flatten array by row
                    item = reshape(fliplr(rot90(item, -1)), 1, []);
                    % write each element with name as index
                    for x = 1:length(item)
                        if ~isempty(item{x})
                            ele_name = char(strcat(mdfHdf5.ARRAY_ELE_BASE_NAME, join(strtrim(split(num2str(index), '  ')), 'x')));
                            ele_path = strcat(new_path, '/', ele_name);
                            [updated_mdf_hdf5_types, error_msg] = construct_hierarchy(struct(ele_name, item{x}), group, ele_path, mdf_hdf5_types, check);
                            mdf_hdf5_types = combine_mdf_hdf5_types(mdf_hdf5_types, updated_mdf_hdf5_types);
                            if ~isempty(error_msg)
                                errors = [errors, error_msg];
                            end
                        end
                        index = increment_index(index, max_index);
                    end
                    H5G.close(group)
                end
            % write empty dataset for empty cell
            else
                mdf_hdf5_types.(x_path) = mdfHdf5.MDF_HDF5_ARRAY_UNTYPED;
                write_attr(hdf5, name, mdfHdf5.EMPTY_CELL_ARRAY, mdfHdf5.HDF5_STRING);
            end
    
        % check if is struct (nested object)
        elseif isstruct(item)
            if isempty(fieldnames(item)) && ~strcmp(new_path, mdfHdf5.MDF_HDF5_TYPES_PATH)
                write_attr(hdf5, name, mdfHdf5.EMPTY_STRUCT, mdfHdf5.HDF5_STRING);
            else
                % Create group to build the next json object on
                group = create_or_open_group(new_path, name, hdf5);
                % Check if struct is an array of structs
                if len > 1 || strcmp(map, mdfHdf5.MDF_HDF5_ARRAY_DICT)
                    % use base name to indicate the original array form in mdf
                    for x = 1:len
                        ele_number = get_ele_number(x-1, len);
                        % build full path for element
                        ele_name = strcat(mdfHdf5.ARRAY_ELE_BASE_NAME, ele_number);
                        array_path = strcat(new_path, '/', ele_name);
                        % create a subgroup for element
                        sub_group = create_or_open_group(array_path, ele_name, group);
                        % write element
                        [updated_mdf_hdf5_types, error_msg] = construct_hierarchy(item(x), sub_group, array_path, mdf_hdf5_types, check);
                        mdf_hdf5_types = combine_mdf_hdf5_types(mdf_hdf5_types, updated_mdf_hdf5_types);
                        if ~isempty(error_msg)
                            errors = [errors, error_msg];
                        end
                        H5G.close(sub_group)
                    end
                    % handle scalar struct
                else
                    if strcmp(new_path, mdfHdf5.MDF_HDF5_TYPES_PATH)
                        % stop checking for types
                        construct_hierarchy(item, group, new_path, mdf_hdf5_types, false);
                    else
                        [mdf_hdf5_types, error_msg] = construct_hierarchy(item, group, new_path, mdf_hdf5_types, check);
                        if ~isempty(error_msg)
                            errors = [errors, error_msg];
                        end
                    end
                end
                H5G.close(group);
            end
        else
            error_msg = strcat('Unhandled type  @ ', new_path);
            errors{length(errors)+1} = error_msg;
        end
        % reset mapping to none
        mapping = false;
        map = '';
    end
end

