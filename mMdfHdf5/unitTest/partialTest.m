classdef partialTest < matlab.unittest.TestCase
    
    properties
        WRITE
        READ
    end
    
    methods (TestClassSetup)
        function load_mdfHdf5(testCase)
            addpath('../../mMdfHdf5');
            testCase.WRITE = 'testOutput/partial_write_mat.hdf5';
            testCase.READ = 'testOutput/partial_read_mat.hdf5';
            
            mdf = struct('a', 'some string');
            mdfHdf5.write(mdf, testCase.WRITE, true);
            mdf = struct('c', struct('c1', 10), 'd', struct('d1', 10));
            mdfHdf5.write(mdf, testCase.READ, true);
        end
    end
    
    methods (Test)
        function test_write_b(testCase)
            paths = 'b';
            mdf = struct('b', 10);
            errors = mdfHdf5.write_partial(mdf, paths, testCase.WRITE, []);
            testCase.verifyEmpty(errors)
            mdf = mdfHdf5.read(testCase.WRITE);
            testCase.verifyEqual(mdf.b, 10);
        end
        function test_write_c(testCase)
            paths = 'c';
            mdf = struct('c', struct('c1', 10, 'c2', 'some string'));
            mdf_hdf5_types = struct('c_x_c1', 'MDF_HDF5_NUM');
            errors = mdfHdf5.write_partial(mdf, paths, testCase.WRITE, mdf_hdf5_types);
            testCase.verifyEmpty(errors)
            mdf = mdfHdf5.read(testCase.WRITE);
            testCase.verifyEqual(mdf.c.c1, 10);
            testCase.verifyEqual(mdf.mdf_hdf5_types.c_x_c1, 'MDF_HDF5_NUM');
        end
        function test_overwrite_c(testCase)
            paths = 'c';
            mdf = struct('c', struct('c1', 23, 'c2', 'some string'));
            mdf_hdf5_types = struct('c_x_c1', 'MDF_HDF5_NUM');
            errors = mdfHdf5.write_partial(mdf, paths, testCase.WRITE, mdf_hdf5_types);
            mdf = mdfHdf5.read(testCase.WRITE);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(mdf.c.c1, 23);
            testCase.verifyEqual(mdf.mdf_hdf5_types.c_x_c1, 'MDF_HDF5_NUM');
        end
        function test_write_d_e(testCase)
            paths = {'d', 'e'};
            mdf = {struct('d', [1 2 3]), struct('e', [1; 2; 3])};
            mdf_hdf5_types = struct('d', 'MDF_HDF5_ARRAY_NUM_UNI', 'e', 'MDF_HDF5_NARRAY_NUM');
            errors = mdfHdf5.write_partial(mdf, paths, testCase.WRITE, mdf_hdf5_types);
            mdf = mdfHdf5.read(testCase.WRITE);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(mdf.d, [1 2 3]);
            testCase.verifyEqual(mdf.mdf_hdf5_types.d, 'MDF_HDF5_ARRAY_NUM_UNI');
            testCase.verifyEqual(mdf.mdf_hdf5_types.e, 'MDF_HDF5_NARRAY_NUM');
        end
        function test_read_c(testCase)
            paths = 'c';
            mdf = mdfHdf5.read_partial(paths, testCase.READ);
            testCase.verifyEqual(mdf.c.c1, 10);
        end
        function test_read_c_d(testCase)
            paths = {'c', 'd'};
            mdf = mdfHdf5.read_partial(paths, testCase.READ);
            testCase.verifyEqual(mdf{1}.c.c1, 10);
            testCase.verifyEqual(mdf{2}.d.d1, 10);
        end

    end
    
end