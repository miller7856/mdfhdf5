classdef readTest < matlab.unittest.TestCase
 
    properties
        TEST
        mdf
    end
 
    methods(TestClassSetup)
        function load_mdf(testCase)
            addpath('../../mMdfHdf5');
            testCase.TEST = 'testOutput/read_py.hdf5';
            testCase.mdf = mdfHdf5.read(testCase.TEST);
        end
    end
    
    methods (Test)
        function test_a(testCase)
            data = testCase.mdf.a;
            testCase.verifyEqual(data, char('some string'));
        end
        function test_b(testCase)
            data = testCase.mdf.b;
            testCase.verifyEqual(data, double(10));
        end
        function test_c(testCase)
            data = testCase.mdf.c;
            arr = [1 2 3; 4 5 6; 7 8 9];
            arr(:, :, 2) = [10 11 12; 13 14 15; 16 17 18];
            testCase.verifyEqual(data, arr);
        end
        function test_c_num_as_list(testCase)
            data = testCase.mdf.c_num_as_list;
            arr = [1 2 3; 4 5 6; 7 8 9];
            arr(:, :, 2) = [10 11 12; 13 14 15; 16 17 18];
            testCase.verifyEqual(data, arr);
        end
        function test_d(testCase)
            data = testCase.mdf.d;
            arr = [1 2 3];
            testCase.verifyEqual(data, arr);
        end
        function test_e(testCase)
            data = testCase.mdf.e;
            arr = [1; 2; 3];
            testCase.verifyEqual(data, arr);
        end
        function test_f(testCase)
            data = testCase.mdf.f;
            arr = [1];
            testCase.verifyEqual(data, arr);
        end
        function test_g(testCase)
            data = testCase.mdf.g;
            arr = [1 2 3; 4 5 6];
            arr(:,:,2) = [10 11 12; 13 14 15];
            testCase.verifyEqual(data, arr);
        end
        function test_h(testCase)
            data = testCase.mdf.h;
            arr = [];
            testCase.verifyEqual(data, arr);
        end
        function test_i(testCase)
            data = testCase.mdf.i;
            arr = {'hello', 'world', '!'};
            testCase.verifyEqual(data, arr);
        end
        function test_j(testCase)
            data = testCase.mdf.j;
            arr = {'hello'};
            testCase.verifyEqual(data, arr);
        end
        function test_k(testCase)
            data = testCase.mdf.k;
            arr = {'hi', 'hey'; 'yo', 'hello'};
            testCase.verifyEqual(data, arr);
        end
        function test_l(testCase)
            data = testCase.mdf.l;
            arr = {'hi'; 'yo'; 'bye'};
            testCase.verifyEqual(data, arr);
        end
        function test_p(testCase)
            data = testCase.mdf.p;
            arr = {[1:3], [4:6]};
            testCase.verifyEqual(data, arr);
        end
        function test_q(testCase)
            data = testCase.mdf.q;
            arr = {[1:3], [], [4:6], []};
            testCase.verifyEqual(data, arr);
        end
        function test_r(testCase)
            data = testCase.mdf.r;
            arr = {[1:3], [4:6]; [7:9], [10:12]};
            testCase.verifyEqual(data, arr);
        end
        function test_s(testCase)
            data = testCase.mdf.s;
            arr = {[1:3], []; [4:6], [7:9]; [], [10:12]};
            testCase.verifyEqual(data, arr);
        end
        function test_t(testCase)
            data = testCase.mdf.t;
            arr = {[1:3], 'hello world', 10, {'hello', 'world', '!'}};
            testCase.verifyEqual(data, arr);
        end
    end
end