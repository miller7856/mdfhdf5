classdef writeTest < matlab.unittest.TestCase
 
    properties
        TEST
    end
 
    methods(TestClassSetup)
        function load_mdf(testCase)
            addpath('../../mMdfHdf5');
            testCase.TEST = 'testOutput/write_mat.hdf5';
        end
    end
    
    methods (Test)
        function test_str(testCase)
            data = 'some string';
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end 
        function test_int(testCase)
            data = 10;
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [1 1]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end  
        function test_1x1_str(testCase)
            data = {'hi'};
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [1 1]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_1x3_str(testCase)
            data = {'hi', 'yo', 'bye'};
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [1 3]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_3x1_str(testCase)
            data = {'hi'; 'yo'; 'bye'};
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [3 1]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_2x2_str(testCase)
            data = {'hi', 'hello'; 'yo' 'bye'};
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [2 2]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
         function test_3x2_str(testCase)
            data = {'sup', 'hey'; 'hi', 'hello'; 'yo' 'bye'};
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [3 2]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
         end
         function test_3x2x2_str(testCase)
            data = {'sup', 'hey'; 'hi', 'hello'; 'yo' 'bye'};
            data(:,:,2) = {'sp', 'hy'; 'h', 'hll'; 'y' 'by'};
            mdf_in = struct('mdf', []);
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [3 2 2]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
         end
         function test_empty(testCase)
            mdf_in = struct('data', []);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [0 0]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
         end
         function test_1x1(testCase)
            mdf_in = struct('data', [1]);
            mdf_in.('mdf_hdf5_types') = struct('data', 'MDF_HDF5_ARRAY_NUM');
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [1 1]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_1x3(testCase)
            mdf_in = struct('data', [1 2 3]);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [1 3]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_3x1(testCase)
            mdf_in = struct('data', [1; 2; 3]);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [3 1]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_2x3(testCase)
            mdf_in = struct('data', [1 2 3; 4 5 6;]);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [2 3]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_3x2(testCase)
            mdf_in = struct('data', [1 2; 3 4; 5 6]);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [3 2]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_3x2x2(testCase)
            data = [1 2; 3 4; 5 6];
            data(:,:,2) = [7 8; 9 10; 11 12];
            mdf_in = struct('data', data);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [3 2 2]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_3x3x3(testCase)
            data = [1 2 3; 4 5 6; 7 8 9];
            data(:,:,2) = [10 11 12; 13 14 15; 16 17 18];
            data(:,:,3) = [19 20 21; 22 23 24; 25 26 27];
            mdf_in = struct('data', data);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), [3 3 3]);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_2x3x4x2(testCase)
            dims = [2 3 4 2];
            data = randn(dims);
            mdf_in = struct('data', data);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), dims);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_3x5x2x6(testCase)
            dims = [3 5 2 6];
            data = randn(dims);
            mdf_in = struct('data', data);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), dims);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_3x2x4x6x2(testCase)
            dims = [3 2 4 6 2];
            data = randn(dims);
            mdf_in = struct('data', data);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), dims);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_5x3x3x4x2x4(testCase)
            dims = [5 3 3 4 2 4];
            data = randn(dims);
            mdf_in = struct('data', data);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), dims);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_5x3x3x4x2x4x3(testCase)
            dims = [5 3 3 4 2 4 3];
            data = randn(dims);
            mdf_in = struct('data', data);
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(size(mdf_in.data), dims);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_untyped_1x2(testCase)
            data = {[1:3], [4:7]};
            mdf_in = struct('mdf_hdf5_types', struct('data', 'MDF_HDF5_ARRAY_UNTYPED'));
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_untyped_1x4(testCase)
            data = {[1:3], 10, [4:7], 'hi'};
            mdf_in = struct('mdf_hdf5_types', struct('data', 'MDF_HDF5_ARRAY_UNTYPED'));
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            testCase.verifyEmpty(errors)
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_untyped_3x2(testCase)
            data = {[1:3], []; [4:6], [7:9]; [], [10:12]};
            mdf_in = struct('mdf_hdf5_types', struct('data', 'MDF_HDF5_ARRAY_UNTYPED'));
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_untyped_3x2x2(testCase)
            data = {[0 0 0], [0 0 1]; [0 1 0], [0 1 1]; [0 2 0], [0 2 1]};
            data(:,:,2) = {[1 0 0], [1 0 1]; [1 1 0], [1 1 1]; [1 2 0], [1 2 1]};
            mdf_in = struct('mdf_hdf5_types', struct('data', 'MDF_HDF5_ARRAY_UNTYPED'));
            mdf_in.('data') = data;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_untyped_2x2x2x3(testCase)
            t = cell(2, 2, 2, 3);
            t(:,:,1,1) = {[1 1], [1 1]; [1 1], [1 1]};
            t(:,:,2,1) = {[2 1], [2 1]; [2 1], [2 1]};
            t(:,:,1,2) = {[1 2], [1 2]; [1 2], [1 2]};
            t(:,:,2,2) = {[2 2], [2 2]; [2 2], [2 2]};
            t(:,:,1,3) = {[1 3], [1 3]; [1 3], [1 3]};
            t(:,:,2,3) = {[2 3], [2 3]; [2 3], [2 3]};
            mdf_in = struct('mdf_hdf5_types', struct('data', 'MDF_HDF5_ARRAY_UNTYPED'));
            mdf_in.('data') = t;
            errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
            mdf_out = mdfHdf5.read(testCase.TEST);
            testCase.verifyEmpty(errors)
            testCase.verifyEqual(mdf_in.data, mdf_out.data);
        end
        function test_untyped_10x1(testCase)
           data = {1; 2; 3; 4; 5; 6; 7; 8; 9; 10};
           mdf_in = struct('mdf_hdf5_types', struct('data', 'MDF_HDF5_ARRAY_UNTYPED'));
           mdf_in.('data') = data;
           errors = mdfHdf5.write(mdf_in, testCase.TEST, true);
           mdf_out = mdfHdf5.read(testCase.TEST);
           testCase.verifyEmpty(errors)
           testCase.verifyEqual(mdf_in.data, mdf_out.data); 
        end
    end
end















