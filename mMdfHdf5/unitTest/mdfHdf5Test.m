import matlab.unittest.TestSuite;

ReadTest = TestSuite.fromFile('readTest.m'); 
WriteTest = TestSuite.fromFile('writeTest.m');
PartialTest = TestSuite.fromFile('partialTest.m');
testSuite =  [ReadTest, WriteTest, PartialTest];

results = run(testSuite);