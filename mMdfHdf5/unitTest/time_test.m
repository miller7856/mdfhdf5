%add mdfHdf5 library to path
addpath('../../mMdfHdf5');

% test files
%data directorty out side of repo, any json works
BIG = '../../../data/gumband_sciatic.json';
OG = '../../testData/OriginalMdfObject.json';
CONSTANT_HDF5 = 'testOutput/time_test_constant_hdf5.hdf5';
PARTIAL_WRITE = 'testOutput/partial_write_mat.hdf5';
FULL_WRITE = 'testOutput/full_write_mat.hdf5';

%load json file, test file is hardcoded here
f = fopen(OG);
str = fread(f, Inf);
fclose(f);
mdf = jsondecode(char(str));

%write mdf to files to ensure they are complete
mdfHdf5.write(mdf, CONSTANT_HDF5, true);
mdfHdf5.write(mdf, PARTIAL_WRITE, true);
mdfHdf5.write(mdf, FULL_WRITE, true);

partial_write_t = 0;
full_write_t = 0;
%number of test iterations 
iter = 10;

mdf = mdfHdf5.read(CONSTANT_HDF5);
%add data to mdf
%mdf.mdf_def.('new_field') = 'new data';
%mdf.mdf_def.('some_data') = double(1:10000);
partial_mdf = struct('mdf_def', mdf.mdf_def);

for i = 1:iter
    fprintf('\n\n\n');
    partial_mdf = mdfHdf5.read_partial('mdf_def/mdf_children', PARTIAL_WRITE);
    %tic;
    mdfHdf5.write_partial(partial_mdf, 'mdf_def/mdf_children', PARTIAL_WRITE);
    %toc;
    %keep track of time and clean up
    %partial_write_t = partial_write_t + toc;

    %rewrite entire mdf to new file
    %tic;
        %mdfHdf5.write(partial_mdf, FULL_WRITE, false);
    %toc;
    %keep track of time
    %full_write_t = full_write_t + toc;

end

fprintf('Average time for parial write: %f\n', partial_write_t/iter);
fprintf('Average time for full write: %f\n', full_write_t/iter);
diff = (full_write_t/iter)/(partial_write_t/iter);
fprintf('Full write is %f times longer than patrial wrtie. \n', diff);



