#helper libraries
import unittest
import os
import h5py
import sys
import numpy as np
import pprint

PY_TEST_DIR = os.path.join(os.path.dirname(__file__), '..', 'pyMdfHdf5')
sys.path.insert(0, PY_TEST_DIR)
#library under testing 
import mdfHdf5

TEST = 'crossPlatformTest.hdf5'

class test1(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        data = {
            'a': 'some string',
            'b' : 10,
            'e' : [1, 2, 3],
            'f' : [[1, 2, 3],[1, 2, 3]],
            'g' : [1],
            'mdf_hdf5_types': {
                'f': 'MDF_HDF5_ARRAY_NUM'
            }
            
        }
        mdfHdf5.write(data, TEST)

    def test_1(self):
        mdf = mdfHdf5.read(TEST)
        self.assertEqual(mdf['a'], 'some string')
        self.assertEqual(mdf['b'], 10)
        self.assertEqual(mdf['e'].all(), np.array([1, 2, 3]).all())
        self.assertEqual(mdf['f'], [[1, 2, 3],[1, 2, 3]])
        self.assertEqual(mdf['g'], np.array([1]))
        self.assertEqual(mdf['mdf_hdf5_types']['f'], 'MDF_HDF5_ARRAY_NUM')

class test2(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        data = {'d': ['some', 'string']}
        mdfHdf5.write_partial(data, 'd', TEST)


    def test_1(self):
        mdf = mdfHdf5.read(TEST)
        self.assertEqual(mdf['a'], 'some string')
        self.assertEqual(mdf['b'], 10)
        self.assertEqual(mdf['c']['c1'].all(), np.array([1, 2, 3]).all())
        self.assertEqual(mdf['c']['c2'].all(), np.array([[1, 2, 3]]).all())
        self.assertEqual(mdf['e'].all(), np.array([1, 2, 3]).all())
        self.assertEqual(mdf['f'], [[1, 2, 3],[1, 2, 3]])
        self.assertEqual(mdf['g'], np.array([1]))
        self.assertEqual(mdf['h'], ['hello', 'world'])
        self.assertEqual(mdf['mdf_hdf5_types']['c_x_c1'], 'MDF_HDF5_NARRAY_NUM_UNI')
        self.assertEqual(mdf['mdf_hdf5_types']['f'], 'MDF_HDF5_ARRAY_NUM')
        self.assertEqual(mdf['mdf_hdf5_types']['h'], 'MDF_HDF5_ARRAY_STR_UNI')

if __name__ == '__main__':
    loader = unittest.TestLoader()
    loader.sortTestMethodsUsing=None
    test = loader.loadTestsFromTestCase(step1)
    runner = unittest.TextTestRunner()
    results = runner.run(test)