from subprocess import Popen
import os
import unittest
import crossPlatformPartialTest
import crossPlatformTest

log = os.path.join(os.path.dirname(__file__), 'log.txt')

"""
*   Test reading and writing back and forth between libraries
"""
test = unittest.defaultTestLoader.loadTestsFromTestCase(crossPlatformPartialTest.test1)
unittest.TextTestRunner().run(test)

p = Popen(['matlab', '-wait', '-nosplash', '-nodesktop', '-logfile', 'log.txt', '-r', 'result = run(crossPlatformPartialTest); disp(result); quit'])
p.wait()

test = unittest.defaultTestLoader.loadTestsFromTestCase(crossPlatformPartialTest.test2)
unittest.TextTestRunner().run(test)  

p = Popen(['matlab', '-wait', '-nosplash', '-nodesktop', '-logfile', 'log.txt', '-r', 'result = run(crossPlatformPartialTest2); disp(result); quit'])
p.wait()

"""
*   Test creating a mdf in python/matlab and reading in matlab/python
"""
test = unittest.defaultTestLoader.loadTestsFromTestCase(crossPlatformTest.write)
unittest.TextTestRunner().run(test)

p = Popen(['matlab', '-wait', '-nosplash', '-nodesktop', '-logfile', 'log.txt', '-r', 'result = run(crossPlatformTest); disp(result); quit'])
p.wait()

test = unittest.defaultTestLoader.loadTestsFromTestCase(crossPlatformTest.read)
unittest.TextTestRunner().run(test)