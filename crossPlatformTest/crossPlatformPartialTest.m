classdef crossPlatformPartialTest < matlab.unittest.TestCase
    
    properties
        mdf
        TEST
    end
    
    methods (TestClassSetup)
        function load_mdfHdf5(testCase)
            addpath('../mMdfHdf5');
            testCase.TEST = 'crossPlatformTest.hdf5';
            testCase.mdf = mdfHdf5.read(testCase.TEST);
        end
    end
    
    methods (Test)
        function test_1(testCase)
           data = testCase.mdf;
           testCase.verifyEqual(data.a, 'some string');
           testCase.verifyEqual(data.b, 10);   
           testCase.verifyEqual(data.e, [1 2 3]);  
           testCase.verifyEqual(data.f, [1 2 3; 1 2 3]);  
           testCase.verifyEqual(data.g, 1);  
           testCase.verifyEqual(data.mdf_hdf5_types.f, 'MDF_HDF5_ARRAY_NUM');
        end
        function test_update(testCase)
           str_array = struct();
           str_array.('h') = {'hello', 'world'};
           data = {struct('c', struct('c1', [1 2 3], 'c2', [1 2 3])), str_array};
           paths = {'c', 'h'};
           mdf_hdf5_types = struct('c_x_c1', 'MDF_HDF5_NARRAY_NUM_UNI', 'h', 'MDF_HDF5_ARRAY_STR_UNI');
           mdfHdf5.write_partial(data, paths, testCase.TEST, mdf_hdf5_types);            
        end
        function test_2(testCase)
           data = mdfHdf5.read(testCase.TEST);
           testCase.verifyEqual(data.c.c1, [1 2 3]);
           testCase.verifyEqual(data.c.c2, [1 2 3]);   
           testCase.verifyEqual(data.h, {'hello', 'world'});   
           testCase.verifyEqual(data.mdf_hdf5_types.c_x_c1, 'MDF_HDF5_NARRAY_NUM_UNI');
           testCase.verifyEqual(data.mdf_hdf5_types.h, 'MDF_HDF5_ARRAY_STR_UNI');
        end
    end

end