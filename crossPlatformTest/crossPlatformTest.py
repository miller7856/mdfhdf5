#helper libraries
import unittest
import os
import h5py
import sys
import numpy as np
import pprint
from mdf_in_memory_py import get_mdf

PY_TEST_DIR = os.path.join(os.path.dirname(__file__), '..', 'pyMdfHdf5')
sys.path.insert(0, PY_TEST_DIR)
#library under testing 
import mdfHdf5

TEST = 'crossPlatformTest.hdf5'

class write(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        mdf = get_mdf()
        mdfHdf5.write(mdf, TEST)
        cls.mdf = mdfHdf5.read(TEST)

    def test_write(self):
        self.assertEqual(self.mdf['a'], 'some string')
        self.assertEqual(self.mdf['b'], 10)
        self.assertEqual(self.mdf['c'], [1])
        self.assertEqual(self.mdf['d'], ['hello'])
        self.assertEqual(self.mdf['e'], [['hi', 'hey'],['hello', 'yo']])
        self.assertEqual(self.mdf['f'], ['hello', 'world', '!'])
        self.assertEqual(self.mdf['g']['a'], 'some string')
        self.assertEqual(self.mdf['g']['b'], 10)
        self.assertEqual(self.mdf['g']['c']['c'], [1])
        self.assertEqual(self.mdf['h'].all(), np.array([1, 2, 3]).all())
        self.assertEqual(self.mdf['i'].all(), np.array([[1],[2],[3]]).all())
        self.assertEqual(self.mdf['j'].all(), np.array([[[1, 2, 3],[4, 5, 6]],[[7, 8, 9],[10, 11, 12]],[[13, 14, 15],[16, 17, 18]]]).all())
        #self.assertEqual(self.mdf['q'], [[['hello', [98.0, 6.0, 7.0]], [[82.0, 16.0, 39.0], 5]], [['hello', [25.0, 15.0, 9.0]], [[14.0, 9.0, 19.0], 5]], [['hello', [45.0, 61.0, 82.0]], [[33.0, 27.0, 80.0], 5]]])
        self.assertEqual(self.mdf['q'][0][0][0], 'hello')
        self.assertEqual(np.array(self.mdf['q'][0][0][1]).all(), np.array([98.0, 6.0, 7.0]).all())
        self.assertEqual(np.array(self.mdf['q'][0][1][0]).all(), np.array([82.0, 16.0, 39.0]).all())
        self.assertEqual(self.mdf['q'][0][1][1], 5)
        self.assertEqual(self.mdf['mdf_hdf5_types']['g_x_c_x_c'], 'MDF_HDF5_NARRAY_NUM_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['h'], 'MDF_HDF5_NARRAY_NUM_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['b'], 'MDF_HDF5_NUM')
        self.assertEqual(self.mdf['mdf_hdf5_types']['d'], 'MDF_HDF5_ARRAY_STR_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['f'], 'MDF_HDF5_ARRAY_STR_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['g_x_b'], 'MDF_HDF5_NUM')
        self.assertEqual(self.mdf['mdf_hdf5_types']['q'], 'MDF_HDF5_ARRAY_UNTYPED')

class read(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.mdf = mdfHdf5.read(TEST)

    def test_read(self):
        self.assertEqual(self.mdf['a'], 'some string')
        self.assertEqual(self.mdf['b'], 10)
        self.assertEqual(self.mdf['c'], [1])
        self.assertEqual(self.mdf['d'], ['hello'])
        self.assertEqual(self.mdf['e'], [['hi', 'hey'],['hello', 'yo']])
        self.assertEqual(self.mdf['f'], ['hello', 'world', '!'])
        self.assertEqual(self.mdf['g']['a'], 'some string')
        self.assertEqual(self.mdf['g']['b'], 10)
        self.assertEqual(self.mdf['g']['c']['c'], [1])
        self.assertEqual(self.mdf['h'].all(), np.array([1, 2, 3]).all())
        self.assertEqual(self.mdf['i'].all(), np.array([[1],[2],[3]]).all())
        self.assertEqual(self.mdf['j'].all(), np.array([[[1, 2, 3],[4, 5, 6]],[[7, 8, 9],[10, 11, 12]],[[13, 14, 15],[16, 17, 18]]]).all())
        self.assertEqual(self.mdf['k'], [[1, 2, 3, 4, 5]])
        self.assertEqual(self.mdf['l'], [['hello', 'world']])
        self.assertEqual(self.mdf['m'], 100)
        self.assertEqual(self.mdf['p'], [1, 2, 3, 4, 5])
        self.assertEqual(self.mdf['q'][0][0][0], 'hello')
        self.assertEqual(np.array(self.mdf['q'][0][0][1]).all(), np.array([98.0, 6.0, 7.0]).all())
        self.assertEqual(np.array(self.mdf['q'][0][1][0]).all(), np.array([82.0, 16.0, 39.0]).all())
        self.assertEqual(self.mdf['q'][0][1][1], 5)
        self.assertEqual(np.array(self.mdf['n'][0][0]).all(), np.array([1, 2, 3, 4, 5]).all())
        self.assertEqual(self.mdf['n'][0][1], 'hello')
        self.assertEqual(self.mdf['n'][1][0], 10)
        self.assertEqual(np.array(self.mdf['n'][1][1]).all(), np.array([5, 4, 3, 2, 1]).all())
        self.assertEqual(self.mdf['mdf_hdf5_types']['c'], 'MDF_HDF5_NARRAY_NUM_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['g_x_c_x_c'], 'MDF_HDF5_NARRAY_NUM_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['h'], 'MDF_HDF5_NARRAY_NUM_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['b'], 'MDF_HDF5_NUM')
        self.assertEqual(self.mdf['mdf_hdf5_types']['d'], 'MDF_HDF5_ARRAY_STR_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['f'], 'MDF_HDF5_ARRAY_STR_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['g_x_b'], 'MDF_HDF5_NUM')
        self.assertEqual(self.mdf['mdf_hdf5_types']['m'], 'MDF_HDF5_NUM')
        self.assertEqual(self.mdf['mdf_hdf5_types']['k'], 'MDF_HDF5_ARRAY_NUM')
        self.assertEqual(self.mdf['mdf_hdf5_types']['p'], 'MDF_HDF5_ARRAY_NUM_UNI')
        self.assertEqual(self.mdf['mdf_hdf5_types']['q'], 'MDF_HDF5_ARRAY_UNTYPED')
        self.assertEqual(self.mdf['mdf_hdf5_types']['n'], 'MDF_HDF5_ARRAY_UNTYPED')



if __name__ == '__main__':
    loader = unittest.TestLoader()
    loader.sortTestMethodsUsing=None
    test = loader.loadTestsFromTestCase(step1)
    runner = unittest.TextTestRunner()
    results = runner.run(test)