classdef crossPlatformTest < matlab.unittest.TestCase
    
    properties
        mdf
        TEST
    end
    
    methods (TestClassSetup)
        function load_mdfHdf5(testCase)
            addpath('../mMdfHdf5');
            testCase.TEST = 'crossPlatformTest.hdf5';
            testCase.mdf = mdfHdf5.read(testCase.TEST);
        end
    end
    
    methods(TestMethodTeardown)
        function write(testCase)
            testCase.mdf.('p') = [1, 2, 3, 4, 5];
            testCase.mdf.('k') = [1, 2, 3, 4, 5];
            testCase.mdf.('l') = {'hello', 'world'};
            testCase.mdf.('m') = 100;
            testCase.mdf.('n') = {[1:5], 'hello'; 10, [5:1]};
            testCase.mdf.mdf_hdf5_types.('p') = 'MDF_HDF5_ARRAY_NUM_UNI';
            testCase.mdf.mdf_hdf5_types.('k') = 'MDF_HDF5_ARRAY_NUM';
            testCase.mdf.mdf_hdf5_types.('m') = 'MDF_HDF5_NUM';
            mdfHdf5.write(testCase.mdf, testCase.TEST, true);
        end
    end
    
    methods (Test)
        function read(testCase)
            disp(testCase.mdf)
            testCase.verifyEqual(testCase.mdf.a, 'some string')
            testCase.verifyEqual(testCase.mdf.b, 10)
            testCase.verifyEqual(testCase.mdf.c, 1)
            testCase.verifyEqual(testCase.mdf.d, {'hello'})
            testCase.verifyEqual(testCase.mdf.e, {'hi', 'hey'; 'hello', 'yo'})
            testCase.verifyEqual(testCase.mdf.f, {'hello', 'world', '!'})
            testCase.verifyEqual(testCase.mdf.g.a, 'some string')
            testCase.verifyEqual(testCase.mdf.g.b, 10)
            testCase.verifyEqual(testCase.mdf.g.c.c, 1)
            testCase.verifyEqual(testCase.mdf.h, [1 2 3])
            testCase.verifyEqual(testCase.mdf.i, [1; 2; 3])
            q(:,:,1) = {'hello', [98.0, 6.0, 7.0]; [82.0, 16.0, 39.0], 5};
            q(:,:,2) = {'hello', [25.0, 15.0, 9.0]; [14.0, 9.0, 19.0], 5};
            q(:,:,3) = {'hello', [45.0, 61.0, 82.0]; [33.0, 27.0, 80.0], 5};
            testCase.verifyEqual(testCase.mdf.q, q)
            data = [1 2 3; 4 5 6];
            data(:,:,2) = [7 8 9; 10 11 12];
            data(:,:,3) = [13 14 15; 16 17 18];
            testCase.verifyEqual(testCase.mdf.j, data)
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.c, 'MDF_HDF5_NARRAY_NUM_UNI')
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.g_x_c_x_c, 'MDF_HDF5_NARRAY_NUM_UNI')
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.h, 'MDF_HDF5_NARRAY_NUM_UNI')
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.b, 'MDF_HDF5_NUM')
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.d, 'MDF_HDF5_ARRAY_STR_UNI')
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.f, 'MDF_HDF5_ARRAY_STR_UNI')
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.g_x_b, 'MDF_HDF5_NUM')
            testCase.verifyEqual(testCase.mdf.mdf_hdf5_types.q, 'MDF_HDF5_ARRAY_UNTYPED')
        end
    end

end