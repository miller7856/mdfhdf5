import json
import h5py
import numpy as np 
import pprint

# MDF_HDF5_TYPES constants
PATH_INDICATOR = '_x_'
ARRAY_ELE_BASE_NAME = 'mdf_hdf5_ele_'
MDF_HDF5_TYPES = 'mdf_hdf5_types'
MDF_HDF5_ARRAY_DICT = 'MDF_HDF5_ARRAY_DICT'
MDF_HDF5_ARRAY_INT = 'MDF_HDF5_ARRAY_NUM'
MDF_HDF5_NARRAY_INT = 'MDF_HDF5_NARRAY_NUM'
MDF_HDF5_NARRAY_INT_UNI = 'MDF_HDF5_NARRAY_NUM_UNI'
MDF_HDF5_ARRAY_INT_UNI = 'MDF_HDF5_ARRAY_NUM_UNI'
MDF_HDF5_ARRAY_STR = 'MDF_HDF5_ARRAY_STR'
MDF_HDF5_ARRAY_STR_UNI = 'MDF_HDF5_ARRAY_STR_UNI'
MDF_HDF5_INT = 'MDF_HDF5_NUM'
MDF_HDF5_ARRAY_UNTYPED = 'MDF_HDF5_ARRAY_UNTYPED'
DIMENSIONS = 'dimensions'
ERRORS = 'errors'
EMPTY_CELL_ARRAY = 'EMPTY_CELL_ARRAY'
EMPTY_STRUCT = 'EMPTY_STRUCT'

# Helper function for _construct_hierarchy
# get type of first none list element of list
def _get_type(arr):
    element = type(arr)
    if element is list or element is np.ndarray:
        element = _get_type(arr[0]) 
    return element

# Helper function for _construct_hierarchy
# get element of list
def _get_element(arr, index):
    if len(index) > 1:
        element = _get_element(arr[index[0]], index[1:])
    else:
        element = arr[index[0]]
    return element

def _get_shape(data):
    shape = [len(data)]
    while type(data[0]) is list:
        if not type(data[0][0]) is int and not type(data[0][0]) is float:
            shape.append(len(data[0]))
            data = data[0]
        else:
            break
    return shape

def _get_ele_number(num, dimensions):
    num = str(num)
    places = len(num)
    while places < dimensions:
        num = '0'+num
        places = places + 1
    return num

"""
Helper function for converting to hdf5.
Params
    hdf5 [int] - hdf5 file identifier
    struct [dict] - mdf object in memory
    mdf_hdf5_types[dict] - types mapping
    paths [string] - tracks current path in struct
    check [boolean] - indicated whether or not to check for types mapping
Returns {mdf_hdf5_types, errors}
"""
def _construct_hierarchy(struct, hdf5, mdf_hdf5_types, paths, check):
    # init vars to be used throughout function
    path, result, errors, mapping = '', None, [], False

    # iterate through each field of dictionary
    for key in struct:

        # if we get to mdf_Hdf5_types stop checking for types
        if check:
            # build path to check if key has extra info in mdf_hdf5_types
            path = (paths + PATH_INDICATOR + key if paths != '' else key)
            if path in mdf_hdf5_types:
                # set mapping to found mapping in mdf_hdf5_types
                mapping = mdf_hdf5_types[path]

        # get type of current object
        item = type(struct[key])
        # if dict make recursive call
        if item is dict:
            if struct[key] == {}:
                hdf5.attrs.create(key, np.string_(EMPTY_STRUCT))
            else:
                group = hdf5.require_group(key)
                if key == MDF_HDF5_TYPES:
                    _construct_hierarchy(struct[key], group, mdf_hdf5_types, path, False)
                else:
                    result = _construct_hierarchy(struct[key], group, mdf_hdf5_types, path, check)
                    mdf_hdf5_types.update(result[MDF_HDF5_TYPES])
                    errors += result[ERRORS]

        # if basic type make it an attribute
        elif item is str or item is float or item is int or item is np.int32:
            if item is str:
                hdf5.attrs.create(key, np.string_(struct[key]))
            else:
                mdf_hdf5_types[path] = MDF_HDF5_INT
                hdf5.attrs.create(key, float(struct[key]))
        
        # handle untyped list
        elif mapping == MDF_HDF5_ARRAY_UNTYPED:
            # get untyped list as numpy
            arr = struct[key]
            if len(arr) == 0:
                hdf5.attrs.create(key, np.string_(EMPTY_CELL_ARRAY))
            else:
                # create group to build array on
                group = hdf5.require_group(key)
                # get shape of arr as list
                shape = _get_shape(arr)
                # save shape as dimensions dataset on the array group
                arr_shape = np.array(shape,  dtype=np.float64)
                dset = group.require_dataset(DIMENSIONS, shape=arr_shape.shape, dtype=np.float64, compression="gzip")
                dset[:] = arr_shape
                # calculate total number of elements in list
                total_dsets = 1
                for i in range(0, len(arr_shape)):
                    total_dsets = total_dsets*arr_shape[i]
                # determine the max index in list
                max_index = [num-1 for num in shape]
                # initialize first index of list
                index = [0 for num in shape]
                # iterate over untyped list
                for i in range(0, int(total_dsets)):
                    # build name of list element based on index
                    name = ARRAY_ELE_BASE_NAME + 'x'.join([str(num) for num in index])
                    # get next element 
                    ele = _get_element(arr, index)
                    # skip eempty list to save space and time
                    if ele != []:
                        # make recursive vall for element 
                        result = _construct_hierarchy({name: ele}, group, mdf_hdf5_types, path, check)
                        mdf_hdf5_types.update(result[MDF_HDF5_TYPES])
                        errors += result[ERRORS]
                    # clear next_index
                    next_index = []
                    # increment index
                    _increment_index(index, max_index, next_index, index, max_index)
                    # set index to next_index, which is set by _increment_index
                    index = next_index

        # handle lists
        elif item is list or item is np.ndarray:
            # set arr as list
            arr = struct[key]
            # determine arr dimensions 
            dimensions = len(np.array(arr).shape)
            # check that list isn't empty
            if len(arr) > 0:
                # get type of list
                element = _get_type(arr)
                # handle list of dicts
                if element is dict:
                    # add mappig for arr_len = 1
                    if len(arr) == 1:
                        # make sure not to overwrite an existing type
                        if path not in mdf_hdf5_types:
                            mdf_hdf5_types[path] = MDF_HDF5_ARRAY_DICT
                    # create group to build list on
                    group = hdf5.require_group(key)
                    # iterate over list of dicts
                    for i, ele in enumerate(struct[key]):
                        # build ele name based on index
                        ele_number = _get_ele_number(i, dimensions)
                        name = ARRAY_ELE_BASE_NAME + str(i)
                        # create subgroup for each element
                        subGroup = group.require_group(name)
                        # make recursive call for each element
                        result = _construct_hierarchy(ele, subGroup, mdf_hdf5_types, path, check)
                        mdf_hdf5_types.update(result[MDF_HDF5_TYPES])
                        errors += result[ERRORS]

                # write list of strings as dataset
                elif element is str or element is np.bytes_ or element is np.str_:
                    # add mappig for uni-dimensional list
                    if dimensions == 1:
                        # make sure not to overwrite an existing type
                        if path not in mdf_hdf5_types:
                            mdf_hdf5_types[path] = MDF_HDF5_ARRAY_STR_UNI
                    # create list of strings as numpy 
                    data = np.array(arr,  dtype=np.string_)
                    # create special type of string dataset and create dataset and write data
                    dt = h5py.special_dtype(vlen=bytes)
                    dset = hdf5.require_dataset(key, shape=data.shape, dtype=dt)
                    dset[:] = data  

                # create data set for list of numeric values
                elif element is float or element is int or element is np.ndarray or element is np.int64 or element is np.float64 or element is np.int32:
                    # add mappig for uni-dimensional list
                    if dimensions == 1:
                        # make sure not to overwrite an existing type
                        if path not in mdf_hdf5_types:
                            mdf_hdf5_types[path] = MDF_HDF5_NARRAY_INT_UNI
                    # create list as numpy and create dataset and write data
                    data = np.array(arr,  dtype=np.float64)
                    dset = hdf5.require_dataset(key, shape=data.shape, dtype=np.float64, compression="gzip")
                    dset[:] = data
                else:
                    # log any unhandled type to errors list
                    errors.append("ERROR: Unhandled type in a list: " + str(element))
            else:
                #is empty list
                data = h5py.Empty("f")
                hdf5.require_dataset(key, shape=None, dtype=np.float64)
        else:
            # log any unhandled type to errors list 
            errors.append("ERROR: unhandled data type: " + str(item))
        # clear path and mapping in case it doesn't get reset in next iteration
        path, mapping = '', False

    return {MDF_HDF5_TYPES: mdf_hdf5_types, ERRORS: errors}

# Helper function for _convert_to_mdf
# build a list based on shape
def _build_arr(arr_shape):
    arr = []
    if len(arr_shape) > 1:
        for i in range(0,arr_shape[0]):
            arr.append(_build_arr(arr_shape[1:]))
    return arr

# Helper function for _convert_to_mdf
# append element to proper location in list
def _append_array(arr, index, element):
    if len(index) > 1:
        _append_array(arr[index[0]], index[1:], element)
    else:
        arr.append(element)

# Helper function for _convert_to_mdf
# increment the index of a list
def _increment_index(index, max_index, next_index, current_index, end_index):
    if index[0] < max_index[0]:
        if len(index[0:]) == 1:
            next_index.append(index[0] + 1)
        else:
            next_index.append(index[0])
            _increment_index(index[1:], max_index[1:], next_index, current_index, end_index)
    elif index[0] == max_index[0]:
        if len(index[0:]) == 1:
            next_index.append(0)
            for i in range(1, len(current_index)+1):
                if current_index[-i] == end_index[-i]:
                    next_index[-i] = 0
                else:
                    next_index[-i] += 1
                    break
        else:
            next_index.append(index[0])
            _increment_index(index[1:], max_index[1:], next_index, current_index, end_index)

# Helper function for _convert_to_mdf
# get names of a group's datasets and attibutes
def _get_keys(hdf5):
    keys = [key for key in hdf5]
    for key in hdf5.attrs:
        keys.append(key)
    keys.sort()
    return keys

# Helper function for _convert_to_mdf
# properly cast an attribute 
def _handle_attr(at):
    element = type(at)
    # determine attr type and cast appropriately
    if element is np.int32:
        attr = float(at)  
    elif element is np.float64:
        # check if value needs decimal values
        if at.is_integer():
            attr = float(at)
        else:
            attr = float(at)  
    elif element is np.bytes_:        
        attr = at.decode('UTF-8')
        if attr == EMPTY_CELL_ARRAY:
            attr = []
        elif attr == EMPTY_STRUCT:
            attr = {}
    elif element is str:
        attr = at
    elif at == EMPTY_CELL_ARRAY:
        attr = []
    elif at == EMPTY_STRUCT:
        attr = {}
    else:
        # log unhandled attribute types
        print("ERROR: Unhandled type for(@attrs): ", at,  element)
    return attr

"""
Helper function for converting to json.
Params
    hdf5 [int] - hdf5 file identifier
    mdf [dict] - mdf object
    mdf_hdf5_types [dict] - types mapping
Returns
    new_mdf [dict]
"""
def _convert_to_mdf(hdf5, mdf, mdf_hdf5_types):
    mapping = False
    # build path to to check for mapping
    path = hdf5.name[1:].replace('/', PATH_INDICATOR)
    if path in mdf_hdf5_types:
        mapping = mdf_hdf5_types[path]

    # check if hdf5 is a dataset
    if isinstance(hdf5, h5py.Dataset):
        # if dataset isn't empy get data
        if hdf5.shape != None:
             data = np.array(hdf5[:]) 
        # handle empty dataset
        if hdf5.shape == None:
            mdf = []        
        # handle uni dimensional list
        elif mapping == MDF_HDF5_ARRAY_INT_UNI:
            if data.shape == (1, 1):
                mdf = np.squeeze(hdf5[:], 0).tolist()
            elif data.shape == (1,):
                mdf = data.tolist()
            else:
                mdf = np.squeeze(hdf5[:]).tolist()
        # handle list
        elif mapping == MDF_HDF5_ARRAY_INT:
            mdf = data.tolist()
        # handle uni dimensional numpy array
        elif mapping == MDF_HDF5_NARRAY_INT_UNI:
            if data.shape == (1, 1):
                mdf = np.squeeze(hdf5[:], 0)
            elif data.shape == (1,):
                mdf = data
            else:
                mdf = np.squeeze(hdf5[:])
        # default numpy array
        elif mapping == MDF_HDF5_NARRAY_INT or hdf5.dtype == np.float64 or hdf5.dtype == int:
            mdf = data
        # hadnle string dataset
        elif mapping == MDF_HDF5_ARRAY_STR or mapping == MDF_HDF5_ARRAY_STR_UNI or hdf5.dtype == h5py.special_dtype(vlen=bytes):
            if len(data.shape) == 1:
                mdf = [ele.decode('UTF-8')for ele in data.tolist()]
            elif data.shape == (1,1):
                mdf = [ele.decode('UTF-8')for ele in np.squeeze(hdf5[:],0).tolist()]
            elif mapping == MDF_HDF5_ARRAY_STR_UNI:
                if data.shape[0] == 1:
                    mdf = [ele.decode('UTF-8')for ele in np.squeeze(hdf5[:],0).tolist()]
                elif data.shape[1] == 1:
                    mdf = [ele.decode('UTF-8')for ele in np.squeeze(hdf5[:],1).tolist()]
            else:
                mdf = []
                for x in range(0, data.shape[0]):
                    mdf.append([ele.decode('UTF-8')for ele in np.array(hdf5[x][:]).tolist()])
        else:
            # log any unhandled data
            print('Unhandled dataset: ', hdf5.dtype)
    else:    
        # handle groups that belong in a list
        if hdf5.__contains__(ARRAY_ELE_BASE_NAME + '0') or hdf5.__contains__(DIMENSIONS):
            arr = []
            # handle cell array of matrices from matlab
            if mapping == MDF_HDF5_ARRAY_UNTYPED:
                total_dsets = 1
                # check if list is multi dimension
                if len(hdf5[DIMENSIONS]) > 1 or len(hdf5[DIMENSIONS].shape) > 1:
                    # get shape of array from dimensions dataset
                    arr_shape = [int(num) for num in np.squeeze(_convert_to_mdf(hdf5[DIMENSIONS], {}, mdf_hdf5_types).tolist())]
                    # determine max index
                    max_index = [num-1 for num in arr_shape]
                    # init list
                    arr = _build_arr(arr_shape)
                    # determine total number of list
                    for i in range(0, len(arr_shape)):
                        total_dsets = total_dsets*arr_shape[i]
                else:
                    # get length of 1D list
                    total_dsets = int(_convert_to_mdf(hdf5[DIMENSIONS], {}, mdf_hdf5_types).tolist()[0])
                    # determine max index
                    max_index = [total_dsets-1]
                # set key index = 1, to skip over dimensions dataset
                key_index = 1
                # get element names 
                keys = _get_keys(hdf5)
                # create next index to help track if an element is missing, missing elements idicate empty datasets
                next_index = []
                # build list
                for i in range(0, total_dsets):
                    # make sure there are still datasets to open
                    if key_index < len(keys):
                        # get index of dataset from key (name) of dataset
                        index = [int(num) for num in keys[key_index].split('_')[-1].split('x')]
                        # check if new index is the next index
                        for i in range(0, len(index)):
                            if len(next_index) != 0:
                                is_next = True if index[i] == next_index[i] else False
                            else:
                                is_next = True if index[i] == 0 else False
                                next_index = [0 for num in index]
                    else:
                        # no more keys means there is no next index
                        is_next = False
                    # add element to list 
                    if is_next:
                        try:
                            # try to open as dset
                            element = _convert_to_mdf(hdf5[keys[key_index]], {}, mdf_hdf5_types)
                        except:
                            # open as attr
                            element = _handle_attr(hdf5.attrs[keys[key_index]])
                        # add new element to list
                        _append_array(arr, index, element)
                        key_index += 1
                    else:
                        # update index to the last index
                        index = next_index
                        # add empty dataset to list
                        _append_array(arr, next_index, [])
                    # increment the last index and set next_index with it
                    next_index = []
                    _increment_index(index, max_index, next_index, index, max_index)
                mdf = arr 
            else:
                # handle array of dicts
                for key in hdf5:
                    arr.append(_convert_to_mdf(hdf5[key], {}, mdf_hdf5_types))
                mdf = arr
        else:
            # handle attrs
            for attr in hdf5.attrs:
                at = hdf5.attrs[attr]
                mdf[attr] = _handle_attr(at)
            # make recursive call for each sub group
            for key in hdf5:
                mdf[key] = _convert_to_mdf(hdf5[key], {}, mdf_hdf5_types)
    return mdf

def write(mdf, hdf5_filename):
    # indicated whether of not to check for mtype mappings
    check_for_map = True
    # return value true if successful
    status = True
    # start path is empty string 
    start_path = ''
    # log errors
    errors = []
    try:
        # get types dict
        mdf_hdf5_types = mdf[MDF_HDF5_TYPES]
    except:
        # create types dict
        mdf_hdf5_types = {}
        mdf[MDF_HDF5_TYPES] = mdf_hdf5_types
    # create hdf5 file for writing
    f = h5py.File(hdf5_filename, 'w')
    # write file
    result = _construct_hierarchy(mdf, f, mdf_hdf5_types, start_path, check_for_map)
    # update types in case any were added
    mdf_hdf5_types.update(result[MDF_HDF5_TYPES])
    errors += result[ERRORS]
    # write types to file
    result = _construct_hierarchy({MDF_HDF5_TYPES: mdf_hdf5_types}, f, {}, '', False)
    errors += result[ERRORS]
    f.close()
    if len(errors) > 0:
        print(errors)
        status = False
    return status

def read(hdf5_filename):
    mdf_object, mdf_hdf5_types = {}, {}
    with h5py.File(hdf5_filename) as f:
        try:
            # read mdf_hdf5_types first to use when reading the rest of the file
            mdf_hdf5_types = _convert_to_mdf(f[MDF_HDF5_TYPES], {}, {})
        except:
            pass
        # read hdf5 file
        mdf_object = _convert_to_mdf(f, mdf_object, mdf_hdf5_types)
        # update mdf_hdf5_types
        mdf_object[MDF_HDF5_TYPES].update(mdf_hdf5_types)
    return mdf_object

def write_partial(mdf, paths, hdf5_filename, mdf_hdf5_types=None):
    # indicated whether of not to check for mtype mappings
    check_for_map = True
    # start path is empty string 
    start_path = ''
    if mdf_hdf5_types is None:
        mdf_hdf5_types = {}
    f = h5py.File(hdf5_filename)
    # handle writing mulitple parts of mdf
    if type(mdf) is list:
        # iteratre over paths to get destination to write to
        for i, path in enumerate(paths):
            # delete if exist
            try:
                del f['/'+path]
            except:
                pass
            result = _construct_hierarchy(mdf[i], f, mdf_hdf5_types, start_path, check_for_map)
            mdf_hdf5_types.update(result[MDF_HDF5_TYPES])
    # handle single dataset
    else:
        # delete if exist
        try:
            del f['/'+paths]  
        except:
            pass
        result = _construct_hierarchy(mdf, f, mdf_hdf5_types, start_path, check_for_map)
        mdf_hdf5_types.update(result[MDF_HDF5_TYPES])
    # write mdf_hdf5_types to file
    _construct_hierarchy({MDF_HDF5_TYPES: mdf_hdf5_types}, f, {}, start_path, False)
    f.close()
     

def read_partial(paths, hdf5_filename):
    mdf, mdf_builder = [], {}
    temp_mdf = mdf_builder
    with h5py.File(hdf5_filename) as f:
        # get mdf_hdf5_types for reading
        mdf_hdf5_types = _convert_to_mdf(f[MDF_HDF5_TYPES], {}, {})
        # handle multiple reads
        if type(paths) is list:
            for k, path in enumerate(paths):
                fields = path.split('/')
                for i in range(0, len(fields)):
                    #convert last group on path to mdf
                    if i == len(fields)-1:
                        mdf_builder[fields[i]] = _convert_to_mdf(f[path], {}, mdf_hdf5_types)
                    #build dict with path as fields
                    else:
                        mdf_builder[fields[i]] = {}
                        mdf_builder = mdf_builder[fields[i]]
                #combine dictioniares 
                mdf.append(temp_mdf)
                mdf_builder = {}
                temp_mdf = mdf_builder
        else:
            fields = paths.split('/')
            for i in range(0, len(fields)):
                #convert last group on path to mdf
                if i == len(fields)-1:
                    mdf_builder[fields[i]] = _convert_to_mdf(f[paths], {}, mdf_hdf5_types)
                #build dict with path as fields
                else:
                    mdf_builder[fields[i]] = {}
                    mdf_builder = mdf_builder[fields[i]]
            mdf = temp_mdf
    return mdf


