#helper libraries
import unittest
import json
import os
import h5py
import sys
import pprint
import numpy as np

PY_TEST_DIR = os.path.join(os.path.dirname(__file__), '..', '..', 'pyMdfHdf5')
sys.path.insert(0, PY_TEST_DIR)
#library under testing 
import mdfHdf5
arr_1 = [1, 2, 3]
arr_2 = [4, 5, 6]
arr_3 = [7, 8, 9]
arr_4 = [10, 11, 12]
arr_5 = [13, 14, 15]
arr_6 = [16, 17, 18]
TEST = os.path.join(os.path.dirname(__file__), 'testOutput', 'read_py.hdf5')
class readTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pp = pprint.PrettyPrinter()
        cls.mdf = mdfHdf5.read(TEST)
        #pp.pprint(cls.mdf)
        print('Running read test...')
    
    def test_a(self):
        data = self.mdf['a']
        self.assertEqual(type(data), str)
        self.assertEqual(data, 'some string')

    def test_b(self):
        data = self.mdf['b']
        self.assertEqual(type(data), float)
        self.assertEqual(data, 10)

    def test_c(self):
        data = self.mdf['c']
        self.assertEqual(type(data), np.ndarray)
        self.assertEqual(data.all(), np.array([[arr_1, arr_2, arr_3], [arr_4, arr_5, arr_6]]).all())

    def test_c_num(self):
        data = self.mdf['c_num']
        mdf_type = self.mdf['mdf_hdf5_types']['c_num']
        self.assertEqual(type(data), list)
        self.assertEqual(data,  [[arr_1, arr_2, arr_3], [arr_4, arr_5, arr_6]])
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_NUM')

    def test_d(self):
        data = self.mdf['d']
        self.assertEqual(type(data), np.ndarray)
        self.assertEqual(data.all(),  np.array(arr_1).all())

    def test_d_num_uni(self):
        data = self.mdf['d_num_uni']
        mdf_type = self.mdf['mdf_hdf5_types']['d_num_uni']
        self.assertEqual(type(data), list)
        self.assertEqual(data, [1, 2, 3])
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_NUM_UNI')

    def test_d_np_uni(self):
        data = self.mdf['d_np_uni']
        mdf_type = self.mdf['mdf_hdf5_types']['d_np_uni']
        self.assertEqual(type(data), np.ndarray)
        self.assertEqual(data.all(), np.array([1, 2, 3]).all())
        self.assertEqual(mdf_type, 'MDF_HDF5_NARRAY_NUM_UNI')

    def test_e(self):
        data = self.mdf['e']
        self.assertEqual(type(data), np.ndarray)
        self.assertEqual(data.all(),  np.array([[1], [2], [3]]).all())

    def test_e_num_uni(self):
        data = self.mdf['e_num_uni']
        mdf_type = self.mdf['mdf_hdf5_types']['e_num_uni']
        self.assertEqual(type(data), list)
        self.assertEqual(data, [1, 2, 3])
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_NUM_UNI')

    def test_e_np_uni(self):
        data = self.mdf['e_np_uni']
        mdf_type = self.mdf['mdf_hdf5_types']['e_np_uni']
        self.assertEqual(type(data), np.ndarray)
        self.assertEqual(data.all(), np.array([1, 2, 3]).all())
        self.assertEqual(mdf_type, 'MDF_HDF5_NARRAY_NUM_UNI')

    def test_f(self):
        data = self.mdf['f']
        self.assertEqual(type(data), np.ndarray)
        self.assertEqual(data.all(),  np.array([1]).all())

    def test_g(self):
        data = self.mdf['g']
        self.assertEqual(type(data), np.ndarray)
        self.assertEqual(data.all(),  np.array([[arr_1, arr_2], [arr_4, arr_5]]).all())

    def test_h(self):
        data = self.mdf['h']
        self.assertEqual(data,  [])

    def test_i(self):
        data = self.mdf['i']
        self.assertEqual(data,  [['hello', 'world', '!']])

    def test_i_uni(self):
        data = self.mdf['i_uni']
        mdf_type = self.mdf['mdf_hdf5_types']['i_uni']
        self.assertEqual(data,  ['hello', 'world', '!'])
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_STR_UNI')

    def test_j(self):
        data = self.mdf['j']
        self.assertEqual(data,  ['hello'])

    def test_k(self):
        data = self.mdf['k']
        self.assertEqual(data,  [["hi", "hey"], ["yo", "hello"]])

    def test_l(self):
        data = self.mdf['l']
        self.assertEqual(data,   [["hi"], ["yo"], ["bye"]])

    def test_l_uni(self):
        data = self.mdf['l_uni']
        mdf_type = self.mdf['mdf_hdf5_types']['l_uni']
        self.assertEqual(data,  ["hi", "yo", "bye"])
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_STR_UNI')

    # test m, n, o?

    def test_p(self):
        data = self.mdf['p']
        mdf_type = self.mdf['mdf_hdf5_types']['p']
        self.assertEqual(data[0].all(), np.array([1,2,3]).all())
        self.assertEqual(data[1].all(), np.array([4,5,6]).all())
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_UNTYPED')

    def test_q(self):
        data = self.mdf['q']
        mdf_type = self.mdf['mdf_hdf5_types']['q']
        self.assertEqual(data[0].all(), np.array([1,2,3]).all())
        self.assertEqual(data[1], [])
        self.assertEqual(data[2].all(), np.array([4,5,6]).all())
        self.assertEqual(data[3], [])
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_UNTYPED')

    def test_r(self):
        data = self.mdf['r']
        mdf_type = self.mdf['mdf_hdf5_types']['r']
        self.assertEqual(data[0][0].all(), np.array([1,2,3]).all())
        self.assertEqual(data[0][1].all(), np.array([4,5,6]).all())
        self.assertEqual(data[1][0].all(), np.array([7,8,9]).all())
        self.assertEqual(data[1][1].all(), np.array([10,11,12]).all())
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_UNTYPED')

    def test_s(self):
        data = self.mdf['s']
        mdf_type = self.mdf['mdf_hdf5_types']['s']
        self.assertEqual(data[0][0].all(), np.array([1,2,3]).all())
        self.assertEqual(data[0][1], [])
        self.assertEqual(data[1][0].all(), np.array([4,5,6]).all())
        self.assertEqual(data[1][1].all(), np.array([7,8,9]).all())
        self.assertEqual(data[2][0], [])
        self.assertEqual(data[2][1].all(), np.array([10,11,12]).all())
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_UNTYPED')

    def test_t(self):
        data = self.mdf['t']
        mdf_type = self.mdf['mdf_hdf5_types']['t']
        self.assertEqual(data[0].all(), np.array([1,2,3]).all())
        self.assertEqual(data[1], 'hello world')
        self.assertEqual(data[2], float(10))
        self.assertEqual(data[3], ['hello', 'world', '!'])
        self.assertEqual(mdf_type, 'MDF_HDF5_ARRAY_UNTYPED')        

if __name__ == '__main__':
    unittest.main()