#helper libraries
import unittest
import os
import h5py
import sys
import numpy as np
import pprint

PY_TEST_DIR = os.path.join(os.path.dirname(__file__), '..', '..', 'pyMdfHdf5')
sys.path.insert(0, PY_TEST_DIR)
#library under testing 
import mdfHdf5 

pp = pprint.PrettyPrinter()

WRITE = os.path.join(os.path.dirname(__file__), 'testOutput/partial_write_py.hdf5')
READ = os.path.join(os.path.dirname(__file__), 'testOutput/partial_read_py.hdf5')

class partialTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('Running partial test...')
        try:
            os.remove(WRITE)
        except:
            pass
        data = {'a': 'some string'}
        mdfHdf5.write(data, WRITE)
        data = {
            'a': 'some string',
            'b': 10,
            'c': {
                'c1': 10,
                'c2': 'some string'
            },
            'd': [1, 2, 3],
            'e': {
                'd': [1, 2, 3]
            }
        }
        mdfHdf5.write(data, READ)

    # partial write to a path that doesn't exist
    def test_write_b(self):
        b = {'b': 10}
        paths = 'b'
        mdfHdf5.write_partial(b, paths, WRITE)
        mdf = mdfHdf5.read(WRITE)
        self.assertEqual(mdf['a'], 'some string')
        self.assertEqual(mdf['b'], 10)

    # partial write to mulitple paths that doesn't exist
    def test_write_c(self):
        c = [{ 'c' : {'c1': 10}}, {'c': {'c2': 'some string'}}]
        paths = ['c/c1', 'c/c2']
        mdfHdf5.write_partial(c, paths, WRITE)
        mdf = mdfHdf5.read(WRITE)
        self.assertEqual(mdf['c']['c1'], 10)
        self.assertEqual(mdf['c']['c2'], 'some string')
        self.assertEqual(mdf['mdf_hdf5_types']['c_x_c1'], 'MDF_HDF5_NUM')

    def test_write_f_k(self):
        f = [{ 'f' : {'f': 10}}, {'k': {'k': 10}}]
        paths = ['f/f', 'k/k']
        mdfHdf5.write_partial(f, paths, WRITE)
        mdf = mdfHdf5.read(WRITE)
        self.assertEqual(mdf['mdf_hdf5_types']['f_x_f'], 'MDF_HDF5_NUM')
        self.assertEqual(mdf['mdf_hdf5_types']['k_x_k'], 'MDF_HDF5_NUM')

    # partial write with mdf_hdf5_types
    def test_write_d(self):
        d = {'d': [1, 2, 3]}
        paths = 'd'
        mdf_hdf5_types = {'d': 'MDF_HDF5_ARRAY_NUM'}
        mdfHdf5.write_partial(d, paths, WRITE, mdf_hdf5_types=mdf_hdf5_types)
        mdf = mdfHdf5.read(WRITE)
        self.assertEqual(type(mdf['d']), list)
        self.assertEqual(mdf['d'], [1, 2, 3])
        self.assertEqual(mdf['mdf_hdf5_types']['d'], 'MDF_HDF5_ARRAY_NUM')

    def test_read_c(self):
        paths = 'c'
        mdf = mdfHdf5.read_partial(paths, READ)
        self.assertEqual(mdf['c']['c1'], 10)

    def test_read_e_c(self):
        paths = ['c', 'e']
        mdf = mdfHdf5.read_partial(paths, READ)
        self.assertEqual(type(mdf), list)
        self.assertEqual(mdf[0]['c']['c2'], 'some string')
        self.assertEqual(mdf[1]['e']['d'].all(), np.array([1, 2,3]).all())

    

if __name__ == '__main__':
    loader = unittest.TestLoader()
    loader.sortTestMethodsUsing=None
    test = loader.loadTestsFromTestCase(partialTest)
    runner = unittest.TextTestRunner()
    results = runner.run(test)