#helper libraries
import unittest
import json
import os
import h5py
import sys
import pprint
import numpy as np

PY_TEST_DIR = os.path.join(os.path.dirname(__file__), '..', '..', 'pyMdfHdf5')
sys.path.insert(0, PY_TEST_DIR)
pp = pprint.PrettyPrinter()
#library under testing 
import mdfHdf5
arr_1 = [1, 2, 3]
arr_2 = [4, 5, 6]
arr_3 = [7, 8, 9]
arr_4 = [10, 11, 12]
arr_5 = [13, 14, 15]
arr_6 = [16, 17, 18]
TEST = os.path.join(os.path.dirname(__file__), 'testOutput', 'write_py.hdf5')
class writeTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('Running write test...')
        
    def test_num(self):
        mdf_in = {'data': 1 }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'], mdf_out['data'])
    
    def test_str(self):
        mdf_in = {'data': 'some string' }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'], mdf_out['data'])

    def test_3_str(self):
        mdf_in = {'data': ['hi', 'yo', 'bye'] }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'], mdf_out['data'])

    # how to handle numpy strings?
    def test_3_str_np(self):
        mdf_in = {'data': np.array(['hi', 'yo', 'bye']) }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(['hi', 'yo', 'bye'], mdf_out['data'])

    def test_3x1_str(self):
        mdf_in = {'data': [['hi'], ['yo'], ['bye']] }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'], mdf_out['data'])

    def test_2x2_str(self):
        mdf_in = {'data': [['hi', 'yo'], ['bye', 'cya']] }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'], mdf_out['data'])

    def test_3(self):
        mdf_in = {'data': np.array(arr_1, dtype=np.float64) }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_1x3(self):
        mdf_in = {'data': np.array([arr_1], dtype=np.float64) }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_3x1(self):
        mdf_in = { 'data': np.array([[1], [2], [3]], dtype=np.float64) }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_2x3(self):
        mdf_in = {'data': np.array([arr_1, arr_2], dtype=np.float64) }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_3x3(self):
        mdf_in = {'data': np.array([arr_1, arr_2, arr_3], dtype=np.float64) }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_3x3x2(self):
        data = np.array(np.random.randint(0, 100, size=(3, 3, 2)), dtype=np.float64)
        mdf_in = {'data': data }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_3x3x2x4(self):
        data = np.array(np.random.randint(0, 100, size=(3, 3, 2, 4)), dtype=np.float64)
        mdf_in = {'data': data }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_3x3x2x4x3(self):
        data = np.array(np.random.randint(0, 100, size=(3, 3, 2, 4, 3)), dtype=np.float64)
        mdf_in = {'data': data }
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'].all(), mdf_out['data'].all())

    def test_untyped_list_3(self):
        data = [np.array([1,2]), np.array([4,5,6]), np.array([7,8,9]), np.array([10,11,12])]
        mdf_hdf5_types = {'data': 'MDF_HDF5_ARRAY_UNTYPED'}
        mdf_in = {'data': data, 'mdf_hdf5_types': mdf_hdf5_types}
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'][0].all(), mdf_out['data'][0].all())
        self.assertEqual(mdf_in['data'][1].all(), mdf_out['data'][1].all())
        self.assertEqual(mdf_in['data'][2].all(), mdf_out['data'][2].all())
        
    def test_untyped_list_2d_2e(self):
        data = [np.array([1,2,3]), [], np.array([4,5,6]), []]
        mdf_hdf5_types = {'data': 'MDF_HDF5_ARRAY_UNTYPED'}
        mdf_in = {'data': data, 'mdf_hdf5_types': mdf_hdf5_types}
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'][0].all(), mdf_out['data'][0].all())
        self.assertEqual(mdf_in['data'][1], mdf_out['data'][1])
        self.assertEqual(mdf_in['data'][2].all(), mdf_out['data'][2].all())
        self.assertEqual(mdf_in['data'][3], mdf_out['data'][3])
        
    def test_untyped_list_2x2(self):
        data = [[np.array([1,2]), np.array([4,5,6])], [np.array([7,8,9]), np.array([10,11,12])]]
        mdf_hdf5_types = {'data': 'MDF_HDF5_ARRAY_UNTYPED'}
        mdf_in = {'data': data, 'mdf_hdf5_types': mdf_hdf5_types}
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'][0][0].all(), mdf_out['data'][0][0].all())
        self.assertEqual(mdf_in['data'][0][1].all(), mdf_out['data'][0][1].all())
        self.assertEqual(mdf_in['data'][1][0].all(), mdf_out['data'][1][0].all())
        self.assertEqual(mdf_in['data'][1][1].all(), mdf_out['data'][1][1].all())

    def test_untyped_list_3x2(self):
        data = [[np.array([1,2,3]), []], [np.array([4,5,6]), []], [np.array([7,8,9]), np.array([10,11,12])]]
        mdf_hdf5_types = {'data': 'MDF_HDF5_ARRAY_UNTYPED'}
        mdf_in = {'data': data, 'mdf_hdf5_types': mdf_hdf5_types}
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'][0][0].all(), mdf_out['data'][0][0].all())
        self.assertEqual(mdf_in['data'][0][1], mdf_out['data'][0][1])
        self.assertEqual(mdf_in['data'][1][0].all(), mdf_out['data'][1][0].all())
        self.assertEqual(mdf_in['data'][1][1], mdf_out['data'][1][1])
        self.assertEqual(mdf_in['data'][2][0].all(), mdf_out['data'][2][0].all())
        self.assertEqual(mdf_in['data'][2][1].all(), mdf_out['data'][2][1].all())

    def test_untyped_list_4(self):
        data = [[np.array([1,2,3]), []], ['hi', 20]]
        mdf_hdf5_types = {'data': 'MDF_HDF5_ARRAY_UNTYPED'}
        mdf_in = {'data': data, 'mdf_hdf5_types': mdf_hdf5_types}
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)
        self.assertEqual(mdf_in['data'][0][0].all(), mdf_out['data'][0][0].all())
        self.assertEqual(mdf_in['data'][0][1], mdf_out['data'][0][1])
        self.assertEqual(mdf_in['data'][1][0], mdf_out['data'][1][0])
        self.assertEqual(mdf_in['data'][1][1], mdf_out['data'][1][1])

    def test_untyped_untyped_3x2x2(self):
        data = [[['hello', [98.0, 6.0, 7.0]], [[82.0, 16.0, 39.0], 5]], [['hello', [25.0, 15.0, 9.0]], [[14.0, 9.0, 19.0], 5]], [['hello', [45.0, 61.0, 82.0]], [[33.0, 27.0, 80.0], 5]]]
        mdf_hdf5_types = {'data': 'MDF_HDF5_ARRAY_UNTYPED'}
        mdf_in = {'data': data, 'mdf_hdf5_types': mdf_hdf5_types}
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)

    def test_untyped_untyped_2x3x2(self):
        data = []
        data.append([ [[0, 0, 0], [0, 0, 1]], [[0, 1, 0], [0, 1, 1]], [[0, 2, 0], [0, 2, 1]] ])
        data.append([ [[1, 0, 0], [1, 0, 1]], [[1, 1, 0], [1, 1, 1]], [[1, 2, 0], [1, 2, 1]] ])
        mdf_hdf5_types = {'data': 'MDF_HDF5_ARRAY_UNTYPED'}
        mdf_in = {'data': data, 'mdf_hdf5_types': mdf_hdf5_types}
        mdfHdf5.write(mdf_in, TEST)
        mdf_out = mdfHdf5.read(TEST)

if __name__ == '__main__':
    unittest.main()