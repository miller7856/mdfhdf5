import h5py
import numpy as np
import os

def create_test_file():
    print('Creating python test file...')
    TEST = os.path.join(os.path.dirname(__file__), 'testOutput', 'read_py.hdf5')
    try:
        os.remove(TEST)
    except:
        pass

    f = h5py.File(TEST)
    mdf_hdf5_types = f.require_group('mdf_hdf5_types')
    f.attrs.create('a', np.string_('some string'))
    f.attrs.create('b', float(10))

    arr_1 = [1, 2, 3]
    arr_2 = [4, 5, 6]
    arr_3 = [7, 8, 9]
    arr_4 = [10, 11, 12]
    arr_5 = [13, 14, 15]
    arr_6 = [16, 17, 18]
    arr_7 = [19, 20, 21]
    arr_8 = [22, 23, 24]
    arr_9 = [25, 26, 27]
    arr_10 = [28, 29, 30]
    arr_11 = [31, 32, 33]
    arr_12 = [34, 35, 36]


    c = np.array([[arr_1, arr_2, arr_3], [arr_4, arr_5, arr_6]])
    dset = f.require_dataset('c', shape=c.shape, dtype=np.float64, compression="gzip")
    dset[:] = c

    c = np.array([[arr_1, arr_2, arr_3], [arr_4, arr_5, arr_6]])
    dset = f.require_dataset('c_num', shape=c.shape, dtype=np.float64, compression="gzip")
    dset[:] = c
    mdf_hdf5_types.attrs.create('c_num',  np.string_('MDF_HDF5_ARRAY_NUM'))

    c_as_list = [[arr_1, arr_2, arr_3], [arr_4, arr_5, arr_6]]
    dset = f.require_dataset('c_num_as_list', shape=c.shape, dtype=float, compression="gzip")
    dset[:] = c_as_list
    mdf_hdf5_types.attrs.create('c_num_as_list',  np.string_('MDF_HDF5_ARRAY_NUM'))

    g = np.array([[arr_1, arr_2], [arr_4, arr_5]])
    dset = f.require_dataset('g', shape=g.shape, dtype=np.float64, compression="gzip")
    dset[:] = g

    d = np.array([arr_1])
    dset = f.require_dataset('d', shape=d.shape, dtype=np.float64, compression="gzip")
    dset[:] = d
    dset = f.require_dataset('d_num_uni', shape=d.shape, dtype=np.float64, compression="gzip")
    dset[:] = d
    mdf_hdf5_types.attrs.create('d_num_uni',  np.string_('MDF_HDF5_ARRAY_NUM_UNI'))
    dset = f.require_dataset('d_np_uni', shape=d.shape, dtype=np.float64, compression="gzip")
    dset[:] = d
    mdf_hdf5_types.attrs.create('d_np_uni',  np.string_('MDF_HDF5_NARRAY_NUM_UNI'))

    e = np.array([[1], [2], [3]])
    dset = f.require_dataset('e', shape=e.shape, dtype=np.float64, compression="gzip")
    dset[:] = e
    dset = f.require_dataset('e_num_uni', shape=e.shape, dtype=np.float64, compression="gzip")
    dset[:] = e
    mdf_hdf5_types.attrs.create('e_num_uni',  np.string_('MDF_HDF5_ARRAY_NUM_UNI'))
    dset = f.require_dataset('e_np_uni', shape=e.shape, dtype=np.float64, compression="gzip")
    dset[:] = e
    mdf_hdf5_types.attrs.create('e_np_uni',  np.string_('MDF_HDF5_NARRAY_NUM_UNI'))

    f_arr = np.array([1])
    dset = f.require_dataset('f', shape=f_arr.shape, dtype=np.float64, compression="gzip")
    dset[:] = f_arr

    data = h5py.Empty("f")
    f.require_dataset('h', shape=None, dtype=np.float64)

    words = [['hello', 'world', '!']]
    data = np.array(words,  dtype=np.string_)
    dt = h5py.special_dtype(vlen=bytes)
    dset = f.require_dataset('i', shape=data.shape, dtype=dt)
    dset[:] = data 

    words = [['hello', 'world', '!']]
    data = np.array(words,  dtype=np.string_)
    dt = h5py.special_dtype(vlen=bytes)
    dset = f.require_dataset('i_uni', shape=data.shape, dtype=dt)
    dset[:] = data
    mdf_hdf5_types.attrs.create('i_uni',  np.string_('MDF_HDF5_ARRAY_STR_UNI'))

    words = ['hello']
    data = np.array(words,  dtype=np.string_)
    dt = h5py.special_dtype(vlen=bytes)
    dset = f.require_dataset('j', shape=data.shape, dtype=dt)
    dset[:] = data 

    words = [["hi", "hey"], ["yo", "hello"]]
    data = np.array(words,  dtype=np.string_)
    dt = h5py.special_dtype(vlen=bytes)
    dset = f.require_dataset('k', shape=data.shape, dtype=dt)
    dset[:] = data 

    words = [["hi"], ["yo"], ["bye"]]
    data = np.array(words,  dtype=np.string_)
    dt = h5py.special_dtype(vlen=bytes)
    dset = f.require_dataset('l', shape=data.shape, dtype=dt)
    dset[:] = data 

    words = [["hi"], ["yo"], ["bye"]]
    data = np.array(words,  dtype=np.string_)
    dt = h5py.special_dtype(vlen=bytes)
    dset = f.require_dataset('l_uni', shape=data.shape, dtype=dt)
    dset[:] = data 
    mdf_hdf5_types.attrs.create('l_uni',  np.string_('MDF_HDF5_ARRAY_STR_UNI'))

    m = np.array([[[arr_1, arr_2, arr_3],[arr_4, arr_5, arr_6]], [[arr_7, arr_8, arr_9],[arr_10, arr_11, arr_12]]])
    dset = f.require_dataset('m', shape=m.shape, dtype=np.float64, compression="gzip")
    dset[:] = m

    n = np.random.random_integers(0, 100, size=(3, 3, 3, 3, 3))
    dset = f.require_dataset('n', shape=n.shape, dtype=np.float64, compression="gzip")
    dset[:] = n

    o = np.random.random_integers(0, 100, size=(2, 2, 2, 2, 2, 2))
    dset = f.require_dataset('o', shape=o.shape, dtype=np.float64, compression="gzip")
    dset[:] = o

    # cell array of 2 datasets
    p = f.require_group('p')
    mdf_hdf5_types.attrs.create('p',  np.string_('MDF_HDF5_ARRAY_UNTYPED'))
    dimensions = np.array([2])
    dset = p.require_dataset('dimensions', dimensions.shape, dtype=np.float64)
    dset[:] = dimensions
    p1 = np.array([1,2,3]) 
    p2 = np.array([4,5,6])
    dset = p.require_dataset('mdf_hdf5_ele_0', p1.shape, dtype=np.float64)
    dset[:] = p1
    dset = p.require_dataset('mdf_hdf5_ele_1', p2.shape, dtype=np.float64)
    dset[:] = p2

    # cell array of 2 datasets and 2 empties
    q = f.require_group('q')
    mdf_hdf5_types.attrs.create('q',  np.string_('MDF_HDF5_ARRAY_UNTYPED'))
    dimensions = np.array([4])
    dset = q.require_dataset('dimensions', dimensions.shape, dtype=np.float64)
    dset[:] = dimensions
    q0 = np.array([1,2,3]) 
    q2 = np.array([4,5,6])
    dset = q.require_dataset('mdf_hdf5_ele_0', q0.shape, dtype=np.float64)
    dset[:] = q0
    dset = q.require_dataset('mdf_hdf5_ele_2', q2.shape, dtype=np.float64)
    dset[:] = q2

    # 2x2 cell array of 4 datasets
    r = f.require_group('r')
    mdf_hdf5_types.attrs.create('r',  np.string_('MDF_HDF5_ARRAY_UNTYPED'))
    dimensions = np.array([2, 2])
    dset = r.require_dataset('dimensions', dimensions.shape, dtype=np.float64)
    dset[:] = dimensions
    r0 = np.array([1,2,3]) 
    r1 = np.array([4,5,6])
    r2 = np.array([7,8,9])
    r3 = np.array([10,11,12])
    dset = r.require_dataset('mdf_hdf5_ele_0x0', r0.shape, dtype=np.float64)
    dset[:] = r0
    dset = r.require_dataset('mdf_hdf5_ele_0x1', r1.shape, dtype=np.float64)
    dset[:] = r1
    dset = r.require_dataset('mdf_hdf5_ele_1x0', r2.shape, dtype=np.float64)
    dset[:] = r2
    dset = r.require_dataset('mdf_hdf5_ele_1x1', r3.shape, dtype=np.float64)
    dset[:] = r3

    # 3x2 cell array of 4 datasets 2 empty
    s = f.require_group('s')
    mdf_hdf5_types.attrs.create('s',  np.string_('MDF_HDF5_ARRAY_UNTYPED'))
    dimensions = np.array([3, 2])
    dset = s.require_dataset('dimensions', dimensions.shape, dtype=np.float64)
    dset[:] = dimensions
    s0 = np.array([1,2,3]) 
    s2 = np.array([4, 5, 6])
    s3 = np.array([7,8,9])
    s5 = np.array([10,11,12])
    dset = s.require_dataset('mdf_hdf5_ele_0x0', s0.shape, dtype=np.float64)
    dset[:] = s0
    dset = s.require_dataset('mdf_hdf5_ele_1x0', s2.shape, dtype=np.float64)
    dset[:] = s2
    dset = s.require_dataset('mdf_hdf5_ele_1x1', s3.shape, dtype=np.float64)
    dset[:] = s3
    dset = s.require_dataset('mdf_hdf5_ele_2x1', s5.shape, dtype=np.float64)
    dset[:] = s5

    # untyped list
    t = f.require_group('t')
    mdf_hdf5_types.attrs.create('t',  np.string_('MDF_HDF5_ARRAY_UNTYPED'))
    dimensions = np.array([4])
    dset = t.require_dataset('dimensions', dimensions.shape, dtype=np.float64)
    dset[:] = dimensions

    t0 = np.array([1,2,3]) 
    dset = t.require_dataset('mdf_hdf5_ele_0', s0.shape, dtype=np.float64)
    dset[:] = s0

    t1 = 'hello world'
    t.attrs.create('mdf_hdf5_ele_1', np.string_(t1))

    t2 = 10
    t.attrs.create('mdf_hdf5_ele_2', float(t2))

    t3 = ['hello', 'world', '!']
    data = np.array(t3,  dtype=np.string_)
    dt = h5py.special_dtype(vlen=bytes)
    dset = t.require_dataset('mdf_hdf5_ele_3', shape=data.shape, dtype=dt)
    dset[:] = data 

    # n-d list
    #n = np.random.random_integers(0, 100, size=(3, 3, 3, 3))

    f.close()
    print('File created.')