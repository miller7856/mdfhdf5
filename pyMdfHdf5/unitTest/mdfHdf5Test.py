import unittest
import partialTest
import readTest
import writeTest
from create_test_file import create_test_file

create_test_file()
test = unittest.defaultTestLoader.loadTestsFromTestCase(partialTest.partialTest)
unittest.TextTestRunner().run(test)
test = unittest.defaultTestLoader.loadTestsFromTestCase(writeTest.writeTest)
unittest.TextTestRunner().run(test)
test = unittest.defaultTestLoader.loadTestsFromTestCase(readTest.readTest)
unittest.TextTestRunner().run(test)