addpath('../mMdfHdf5')
db = 'R:\DB\MDF\cat\bladder';
top_level = 'bladder';

create_folder_structure(db, top_level);

total_size = 0;
total_time = 0;

[size, total_size, total_time] = crawl(db, top_level, total_size, total_time);
add_stats(db, total_size, total_time);



function create_folder_structure(path, top_level)
    folder = dir(path);
    for x = 1:length(folder)
        item = folder(x);
        if item.isdir == 1 && ~strcmp(item.name(1), '.') 
            new_dir = strcat(top_level, '/', item.name);
            disp(new_dir)
            mkdir(new_dir);
            new_path = strcat(path, '\', item.name);
            create_folder_structure(new_path, new_dir);
        end
    end
end


function [size, total_size, total_time] = crawl(folder_path, top_level, total_size, total_time)
    size = 0;
    folder = dir(folder_path); 
    for x = 1:length(folder)
        item = folder(x);
        if item.isdir == 0
            ext = split(item.name, '.');
            if strcmp(ext(end), 'mat')
                 split_path = split(item.folder, '\');
                 for i = 1:length(split_path)
                    if strcmp(split_path(i), top_level)
                       dest = strjoin(split_path(i:end), '\');
                       file = char(strcat(dest, '\', ext(1), '.hdf5'));
                       break  
                    end
                 end
                 path = strcat(item.folder, '\', item.name);
                 size = size + item.bytes;
                 mdf = load(path);
                 fprintf('Converting %s to %s\n', path, file);
                 errors = mdfHdf5.write(mdf, file, true);
                 if ~isempty(errors)
                    fileID = fopen('mMdfH5_errors.txt','a');
                    fprintf(fileID, path);
                    fprintf(fileID, '\n');
                    for i = 1:length(errors)
                       fprintf(fileID, strcat(errors{i}, '\n'));
                    end
                    fclose(fileID);
                    % trying to catch this error!
                    error(path);
                 end
            end
        else
           if ~strcmp(item.name(1), '.') 
                path = strcat(item.folder, '\', item.name);
                fprintf('Crawling %s\n', path);
                % ignore for testing purposes
                if ~strcmp('spkData', item.name)
                    tic
                    [size, update_size, update_time] = crawl(path, top_level, total_size, total_time);
                    time = toc;
                    total_time = update_time + time;
                    total_size = update_size + size;
                    add_stats(path, size, time);
                else
                    fileID = fopen('mMdfH5_errors.txt','a');
                    fprintf(fileID, 'Skipping %s \n', path); 
                    fclose(fileID);
                end
           end
        end
    end
end


function add_stats(file, size, time)
    GB_BYTES = 1073741824;
    KB_BYTES = 1024;
    time = time/60;
    time_str = ' minutes ';
    if time > 60
        time = time/60;
        time_str = ' hours ';
    end
    if size > GB_BYTES/2
        size = size/GB_BYTES;
        size_str = ' GB ';
    else
        size = size/KB_BYTES;
        size_str = ' KB ';
    end
    stat = strcat('SIZE: %.2f', size_str, ' TIME: %.2f ', time_str, ' FILE: %s.  \n ');
    statID = fopen('stats.txt','a');
    fprintf(statID, stat, size, time, file);
    fprintf(statID, '\n');
    fclose(statID);
end






