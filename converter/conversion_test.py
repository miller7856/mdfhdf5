import os
import sys
import unittest
from subprocess import check_output
PY_TEST_DIR = os.path.join(os.path.dirname(__file__), '..', 'pyMdfHdf5')
sys.path.insert(0, PY_TEST_DIR)
import mdfHdf5
import numpy as np

rootDir = 'bladder'
pyDir = rootDir + 'py'

# Step 2 of conversion test
# after step 1 run this to rewrite every hdf5 with pyMdfHdf5
# during the rewrite each property is compared

# Note: for some reson this changed the data modified for each file in rootDir

def create_folder_structure(rootDir, pyDir):
    os.mkdir(pyDir)
    for dirName, subdirList, fileList in os.walk(rootDir):
        print(dirName)
        dirPath = pyDir + '/' + dirName
        os.mkdir(dirPath)


def rewrite_hdf5(rootDir, pyDir):
    for dirName, subdirList, fileList in os.walk(rootDir):
        split_dir = dirName.split('\\')
        if len(split_dir) > 1 and split_dir[1] != 'Albus':
            for fname in fileList:
                py_path = pyDir + "\\" + dirName + "\\" + fname
                hdf5_path = dirName + "\\" + fname
                print('Writing and comparing ', py_path)
                mdf_from_mat = mdfHdf5.read(hdf5_path)
                status = mdfHdf5.write(mdf_from_mat, py_path)
                if not status:
                    fid = open('pyMdfHdf5_errors.txt', 'a')
                    fid.write(hdf5_path)
                    fid.close()
                mdf = mdfHdf5.read(py_path)
                compare(mdf, mdf_from_mat, '', hdf5_path)
                
                    
def compare(mdf, mdf_mat, path, file_path):
    for key in mdf:
        new_path = path + '/' + key
        if type(mdf[key]) is dict:
            compare(mdf[key], mdf_mat[key], new_path, file_path)
        elif type(mdf[key]) is np.ndarray:
            if not mdf[key].all() == mdf_mat[key].all():
                fid = open('py_conversion_errors.txt', 'a')
                fid.write(file_path)
                fid.write(new_path + ' not equal.')
                fid.close()
        elif type(mdf[key]) is list:
            check_list(new_path, mdf[key], mdf_mat[key], file_path)
        elif not mdf[key] == mdf_mat[key]:
            fid = open('py_conversion_errors.txt', 'a' )
            fid.write(file_path)
            fid.write(new_path + ' not equal.')
            fid.close()
            #print('Py ', mdf[key])
            #print('Mat ', mdf_mat[key])
            #input()

def check_list(path, list1, list2, file_path):
    for i, ele in enumerate(list1):
        new_path = path + '/' + str(i)
        if type(list1[i]) is list:
            check_list(new_path, list1[i], list2[i], file_path)
        elif type(list1[i]) is dict:
            compare(list1[i], list2[i], new_path, file_path)
        elif not list1[i] == list2[i]:
            fid = open('py_conversion_errors.txt', 'a')
            fid.write(file_path)
            fid.write(new_path + ' not equal.')
            fid.close()
            #print('Py ', list1[i])
            #print('Mat ', list2[i])
            #input()

# Test single file
#mdf_mat = mdfHdf5.read('test.hdf5')
#mdfHdf5.write(mdf_mat, 'test_py.hdf5')
#mdf = mdfHdf5.read('test_py.hdf5')
#compare(mdf, mdf_mat, '')
        
#create_folder_structure(rootDir, pyDir)
#rewrite_hdf5(rootDir, pyDir)