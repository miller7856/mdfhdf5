addpath('../mMdfHdf5')
db = 'R:\DB\MDF\cat\bladder';
converted_db = 'bladder';
top_level = 'bladder';
% check entire db
crawl(db, top_level, py);

% Step one of conversion test
% Once all file have been converted by convert_to_hdf5
% run this to compare each file

% single test
% bladder\BigRock\eln\eln-f9d4965d-5b4b-4f68-9d2f-8558fd4b3470.hdf5 and R:\DB\MDF\cat\bladder\BigRock\eln\eln-f9d4965d-5b4b-4f68-9d2f-8558fd4b3470.data.mat 
%mdf_mat = load('R:\DB\MDF\cat\bladder\AlexanderKeith\eln\eln-ce7efddd-d5b7-409b-b40b-a20f738e453d.data.mat');
%mdfHdf5.write(mdf_mat, 'test.hdf5', true);
%mdf = mdfHdf5.read('bladderpy\bladder\AlexanderKeith\eln\eln-ce7efddd-d5b7-409b-b40b-a20f738e453d.hdf5');
%compare_mdf(mdf, mdf_mat, 'bladderpy\bladder\AlexanderKeith\eln\eln-ce7efddd-d5b7-409b-b40b-a20f738e453d.hdf5')

function crawl(folder_path, top_level, py)
    folder = dir(folder_path); 
    for x = 1:length(folder)
        item = folder(x);
        if item.isdir == 0
            ext = split(item.name, '.');
            if strcmp(ext(end), 'mat')
                split_path = split(item.folder, '\');
                for i = 1:length(split_path)
                    if strcmp(split_path(i), top_level)
                        dest = strjoin(split_path(i:end), '\');
                        if py == true
                            mdf_path = char(strcat('bladderpy', '\', dest, '\', ext(1), '.hdf5'));
                        else
                            mdf_path = char(strcat(dest, '\', ext(1), '.hdf5'));
                        end
                        break
                    end
                end
                mdf_mat_path = strcat(item.folder, '\', item.name);
                fprintf('Comparing %s and %s \n', mdf_path, mdf_mat_path);
                mdf_mat = load(mdf_mat_path);
                mdf = mdfHdf5.read(mdf_path);
                compare_mdf(mdf, mdf_mat, mdf_path)
            end
        else
           if ~strcmp(item.name(1), '.') && ~strcmp(item.name, 'Albus')%&& ~strcmp('AlexanderKeith', item.name) && ~strcmp('Bacchus', item.name)
                path = strcat(item.folder, '\', item.name);
                crawl(path, top_level, py);
           end
        end
    end
end

function compare_mdf(mdf, mdf_mat, file_path)
    keys = fieldnames(mdf);
    mdf_no_types = struct();
    for i = 1:length(keys)
       if ~strcmp(keys{i}, 'mdf_hdf5_types')
           mdf_no_types.(keys{i}) = mdf.(keys{i});
       end 
    end
    if ~isequal(mdf_no_types, mdf_mat)
        check_fields(mdf_no_types, mdf_mat, '', file_path);
    end
end

function check_fields(mdf, mdf_mat, path, file_path)
    mdf_keys = fieldnames(mdf);
    mdf_mat_keys = fieldnames(mdf_mat);
    if length(mdf_keys) == length(mdf_mat_keys)
        % check struct array
        if isstruct(mdf) && length(mdf) > 1
            for k = 1:length(mdf)
                new_path = strcat(path, '/', num2str(k));
                check_fields(mdf(k), mdf_mat(k), new_path, file_path); 
            end
        else
            % check struct
            for i = 1:length(mdf_keys)
                key = mdf_keys{i};
                item = mdf.(key);
                if isstruct(item)
                    new_path = strcat(path, '/', key);
                    check_fields(mdf.(key), mdf_mat.(key), new_path, file_path);
                else
                    new_path = strcat(path, '/', key);
                    if ~isequal(mdf.(key), mdf_mat.(key)) 
                        full_path = strcat(path, '/', key);
                        fileID = fopen('mat_conversion_errors.txt','a');
                        fprintf(fileID, 'Issue: %s \n', file_path);
                        fprintf(fileID, '%s not equal. \n', full_path);
                        fclose(fileID);
                    end
                end
            end
        end
    else
        err = strcat('Length mdf_keys: ', string(length(mdf_keys)), ' != Length mdf_mat_keys: ', string(length(mdf_mat_keys)));
        fileID = fopen('mat_conversion_errors.txt','a');
        fprintf(fileID, 'Issue: %s \n', file_path);
        fprintf(fileID, '%s  \n', err);
        fclose(fileID);
    end
end