from subprocess import Popen
import os
import conversion_test
import datetime

now = str(datetime.datetime.now())

try:
    os.remove('mat_conversion_errors.txt')
    fid = open('mat_conversion_errors.txt', 'a')
    fid.write(now)
    fid.close()
except:
    fid = open('mat_conversion_errors.txt', 'a')
    fid.write(now)
    fid.close()

try:
    os.remove('mMdfH5_errors.txt')
    fid = open('mMdfH5_errors.txt', 'a')
    fid.write(now)
    fid.close()
except:
    fid = open('mMdfH5_errors.txt', 'a')
    fid.write(now)
    fid.close()

try:
    os.remove('stats.txt')
    fid = open('stats.txt', 'a')
    fid.write(now)
    fid.close()
except: 
    fid = open('stats.txt', 'a')
    fid.write(now)
    fid.close()

try:
    os.remove('py_conversion_errors.txt')
    fid = open('py_conversion_errors.txt', 'a')
    fid.write(now)
    fid.close()
except:
    fid = open('py_conversion_errors.txt', 'a')
    fid.write(now)
    fid.close()

try:
    os.remove('pyMdfHdf5_errors.txt')
    fid = open('pyMdfHdf5_errors.txt', 'a')
    fid.write(now)
    fid.close()
except:
    fid = open('pyMdfHdf5_errors.txt', 'a')
    fid.write(now)
    fid.close()


# convert .mat
# run convert_to_hdf5
p = Popen(['matlab', '-wait', '-nosplash', '-nodesktop', '-r', 'convert_to_hdf5; quit;'])
p.wait()

#compare .mat to .hdf5
p = Popen(['matlab', '-wait', '-nosplash', '-nodesktop', '-r', 'py=false; conversion_test; quit;'])
p.wait()

#convert and compare .hdf5 to rewritten py hdf5
rootDir = 'bladder'
pyDir = rootDir + 'py'
conversion_test.create_folder_structure(rootDir, pyDir)
conversion_test.rewrite_hdf5(rootDir, pyDir)

# compare py hdf5 to original .mat
p = Popen(['matlab', '-wait', '-nosplash', '-nodesktop', '-r', 'py=true; conversion_test; quit;'])
p.wait()